<?php
class Admin_model extends Order_model{
    public function get_companies( Person $person ){
        $return = array();
        if( $person->person_type == Person::TYPE_ADMIN ){
            $filter = new Company();
            $companies = $this->get_object( $filter );
            foreach( $companies as $company ){
                $return[ $company->id ] = $company->name;
            }
        }
        return $return;
    }    
}