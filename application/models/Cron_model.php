<?php
class Cron_model extends Order_model{
    public function change_status(){
        $filter = new Filter();
        $today  = gmdate('Y-m-d', time() - 24*60*60);
        $filter->set_start_date($today);
        $filter->set_end_date($today);        
        $filter->set_status( Order::ORDER_STATUS_DISPATCHED );
        
        
        $orders = $this->get_orders( $filter );
        
        /**
        * We want to update only STATUS column, so we'll use empty object
        * with id and status.
        * 
        * @var Order
        */
        $temp_order = new Order();
        /**
        * 0 is default. ORM updates any field that isnt null, therefore
        * we want to avoid it.
        */
        $temp_order->set_amount_due(null);
        $temp_order->set_paid(null);
        $temp_order->set_subtotal(null);
        $temp_order->set_tax(null);
        $temp_order->set_total(null);
        $temp_order->set_status( Order::ORDER_STATUS_PENDING_REPORT );
        
        $updated_orders = array();
        foreach( $orders as $order ){
            
            $temp_order->set_id( $order->id );
            $this->save_or_update_object( $temp_order );
            $updated_orders[] = $order->id;
        }
        return 'Updated: '.implode(', ',$updated_orders);
    }    
}
