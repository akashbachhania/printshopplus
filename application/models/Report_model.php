<?php
class Report_model extends Order_model{
    
    private $user;
    private $subcategories;
    private $categories;
    private $property_types;
    
    public function set_user($user){
        $this->user = $user;
    }
    public function get_report_form_types(){        
        $filter = new Type();
        $filter->set_type( Type::TYPE_UTILITY_STATUS );
        $types = $this->get_object( $filter );        
        return $types;
    }
    
    public function get_category_for_init( $category_id, $index ){
        $categories = $this->report_model->get_categories_hierarchy($category_id,$category_index);
        $category = $categories->get_category($category_id, $category_index);
        return $category;
    }
    
    public function get_categories_hierarchy($category_id = null, $index = null){
        $categories = $category_id ? array($category_id=>$this->get_category($category_id)) : $this->get_categories();
        $index = $index ? $index : 0;
        
        $return_categories = array();
        foreach( $categories as $category_id=>$category ){ 
            $subcategories = $this->get_subcategories_for_category($category_id);
            /**
            * @var Category
            */
            //$category->add_general_descriptions($this->get_subcategories_for_category($category_id, true ));
            $category->add_subcategories($subcategories);
            $category->_index = $index;
            $return_categories[$category_id][$index] = $category;
        }
        return new Categories($return_categories);
    }
    
    public function get_categories(){
        if( !$this->categories ){
            $categories = array_merge( 
                $this->get_object( new Category( null,null,Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)), 
                $this->get_object( new Category( null,null,$this->user->company_id, Record::ACTIVE_RECORD))
            );    
            $sorted = array();
            foreach($categories as $category){
                $sorted[$category->id]=$category;
            }
            $this->categories = $sorted;
        }
        return $this->categories;
    }
    
    public function get_categories_for_dropdown(){
        $categories = array();
        foreach($this->get_categories() as $category_id=>$category){
            $categories[$category_id] = $category->name;
        }
        return $categories;
    }
        
    public function get_subcategories(){       
        if( !$this->subcategories ){             
            $subcategories = array_merge( 
                $this->get_object( new Subcategory(null,null,null,Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)), 
                $this->get_object( new Subcategory(null,null,null,$this->user->company_id, Record::ACTIVE_RECORD ))
            ); 
            $values = array_merge( 
                $this->get_object( new Value(null,null,null,Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)), 
                $this->get_object( new Value(null,null,null,$this->user->company_id, Record::ACTIVE_RECORD ))
            );
            
            $sub_cats = array();
            foreach( $subcategories as $subcategory ){
                if( !isset($sub_cats[$subcategory->category_id])){
                     $sub_cats[$subcategory->category_id] = array();
                }
                $values_array = array();
                foreach($values as $value ){
                    /**
                    * @var Value
                    */
                    if( $value->subcategory_id == $subcategory->id ){
                        $values_array[$value->id] = $value;                        
                    }
                }
                $subcategory->add_values($values_array);
                $sub_cats[$subcategory->category_id][$subcategory->id] = $subcategory;
            }
            $this->subcategories = $sub_cats;
        }
        return $this->subcategories;
    }    
    
    public function get_subcategories_for_category($category_id, $general_description_type = false){
        $subcategories = array_merge( 
            $this->get_object( new Subcategory(null, null, $category_id,Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD, null)), 
            $this->get_object( new Subcategory(null, null, $category_id,$this->user->company_id, Record::ACTIVE_RECORD, null ))
        );
        $ret_subcategories = array();
        foreach( $subcategories as $subcategory ){
            $values = array_merge( 
                $this->get_object( new Value(null, null, $subcategory->id, Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)), 
                $this->get_object( new Value(null, null, $subcategory->id, $this->user->company_id, Record::ACTIVE_RECORD ))
            );
            $ret_values = array();
            foreach( $values as $value){
                $concerns = $this->get_concerns( $value->id);
                $value->add_concerns( $concerns );
                $ret_values[$value->id] = $value;
            }
            $subcategory->add_values($ret_values);
            $ret_subcategories[$subcategory->id] = $subcategory;
        }
        return $ret_subcategories;    
    }
    
    public function get_category($category_id){
        $category = null;
        if( (!$category = $this->get_object(new Category($category_id,null, Record::SYSTEM_COMPANY_ID ), true)) && 
            (!$category = $this->get_object(new Category($category_id,null, $this->user->company_id), true)) ){
               throw new Expection('Category doesnt exist');                     
        } 
        return $category;
    }
    
    public function get_template( $template_id ){
        $template = null;
        if((!$template = $this->get_object( new Template(null, null, null, null, $template_id, Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD), true))&&
           (!$template = $this->get_object( new Template(null, null, null, null, $template_id, $this->user->company_id, Record::ACTIVE_RECORD), true))){
            throw new Exception('Template doesnt exist');
        }
        return $template;                 
    }

    public function get_concerns($value_id){
        $concerns = array_merge( 
            $this->get_object( new Concern(null ,null, $value_id, Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)), 
            $this->get_object( new Concern(null, null, $value_id, $this->user->company_id, Record::ACTIVE_RECORD ))
        );
             
        if( !$concerns ){
            for( $i=0;$i<5;$i++){
                $concerns[$value_id.$i] = new Concern($value_id.$i, 'Mock concern '.$i, $value_id, $this->user->company_id, Record::ACTIVE_RECORD);
            }        
        }
        return $concerns;
    }        
    
    public function get_properties( $group_for_dropdown = false){
        if( !$this->property_types ){
            $this->property_types = array_merge( 
                $this->get_object( new Property(null, null, null, Record::SYSTEM_COMPANY_ID, Record::ACTIVE_RECORD)), 
                $this->get_object( new Property(null, null, null, $this->user->company_id, Record::ACTIVE_RECORD ))
            );             
        }
        return $this->property_types;
    }
    public function get_properties_for_dropdown(){
        $properties = $this->get_properties();
        $temp = array();
        foreach($this->property_types as $property ){
            $temp[$property->id] = $property->name;
        }
        return $temp;             
    }
    
    public function get_types(){
        if( !isset($this->types) ){
            $filter = new Type();
            $filter->set_type( Type::TYPE_UTILITY_STATUS );
             
            $types = $this->get_object( $filter );   
            $types = array_merge(
                array(new Type('add_new_item_type', null,'add_utility',null, Type::TYPE_UTILITY_STATUS)),
                //array(new Type()), 
                $this->get_object(new Type(null, $this->user->company_id, null, null, Type::TYPE_UTILITY_STATUS, Record::ACTIVE_RECORD)), 
                $this->get_object(new Type(null, Record::SYSTEM_COMPANY_ID, null, null, Type::TYPE_UTILITY_STATUS, Record::ACTIVE_RECORD)) 
            );
            $this->types = $types;            
        }
        return $this->types;        
    }
    
    public function get_general_disclosures(){
        if( !isset($this->general_disclosures) ){
            $filter = new Type();
            $filter->set_type( Type::TYPE_GENERAL_REMARKS );
            $general_disclosures = array_merge( 
                array(new Type('add_new_item_disclosure',null,'add_disclosure', null,Type::TYPE_GENERAL_REMARKS)), 
                //array(new Type()), 
                $this->get_object(new Type(null, $this->user->company_id, null, null, Type::TYPE_GENERAL_REMARKS, Record::ACTIVE_RECORD)), 
                $this->get_object(new Type(null, Record::SYSTEM_COMPANY_ID, null, null, Type::TYPE_GENERAL_REMARKS, Record::ACTIVE_RECORD))                
            );            
            $this->general_disclosures = $general_disclosures;           
        }
        return $this->general_disclosures;
    }    
    
    
    
        
    public function test(){
        $categories = $this->get_categories_hierarchy();
        $this->test_add_category();
    }
    
    public function test_add_category(){
        $categories = $this->get_categories_hierarchy();
        $category = $this->get_object( new Category(14), true);
        $categories->add_category( $category, 0 );
        $categories->add_category( $category );
    }
    
    public function test_add_subcategory(){
        $categories = $this->get_categories_hierarchy();
        $subcategory = $this->get_object( new Subcategory(42), true);
        $categories->add_subcategory( $subcategory, 0 );
        $value = new Value(null,'Test Value', $subcategory->id, null,null,null);
        $categories->add_subcategory_value( $value, 0 ); 
    }
    
    public function test_add_concern(){
        $categories = $this->get_categories_hierarchy();
        $concern = new Concern(null,'Test concern', 9746, null,null);
        $categories->add_concern( $concern, 43, 0);
        DebugBreak();
    }
    
    public function get_last_undo_object( $object_id ){
        $query = 'SELECT * FROM `dr_report_undo` WHERE object_id="'.$object_id.'" ORDER BY id DESC LIMIT 1';
        $results = $this->db->query( $query );
        return $this->marshall_object($results, new Undo(), true);
    }
    
    public function marshall_object($results, $object, $return_scalar = false){
        
        $class   = get_class( $object );
        $methods = get_class_methods( $class );
        
        $objects = array();
        foreach( $results->result() as $row_object ){
            $object = new $class;
            foreach( $row_object as $field=>$value ){
                $call = 'set_' . $field;
                if( in_array( $call, $methods )){
                    $object->$call( $value );
                }
                
            }
            $object->wake_up();
            $objects[] = $object;   
        }
        return $return_scalar ? reset($objects): $objects;
    }
}
      
