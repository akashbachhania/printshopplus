<?php
class Bootstrap_helper{
    public function get_bootstrap_accordion( $accordion_id, $accordion_elements  ){
        $accordion = '<div class="accordion" id="'.$accordion_id.'">';
        foreach( $accordion_elements as $element) {    
            $heading_class = isset($element['heading_class']) ? $element['heading_class'] : '';
            $body_class    = isset($element['body_class']) ? $element['body_class'] : '';
            $before_text   = isset($element['before_text']) ? $element['before_text'] : '';
            $after_text    = isset($element['after_text']) ? $element['after_text'] : '';
            $accordion .=
            '<div class="accordion-group">
                <div class="accordion-heading '.$heading_class.'">
                    '.$before_text.'
                    <a class="accordion-toggle"  data-toggle="collapse" data-parent="#accordion2" href="#'.$element['href'].'">
                        '.$element['text'].'
                    </a>
                    '.$after_text.'
                </div>
                <div id="'.$element['href'].'" class="accordion-body collapse in '.$body_class.'">
                    <div class="accordion-inner">
                        '.$element['accordion_data'].'
                    </div>
                </div>
            </div>';                 
        }
        $accordion .= '</div>';
        
        return $accordion;
    }
}
