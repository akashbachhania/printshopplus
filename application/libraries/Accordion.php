<?php
class Accordion extends Bootstrap_helper{
    
    const DEFAULT_NULL = 'null';
    const TYPE_CONCERN = 'concern';
    const TYPE_SUBCATEGORY = 'subcategory';
    const TYPE_CATEGORY = 'category';
    const TYPE_VALUE = 'value';
    const TYPE_CONTAINER = 'container';
    
    public function get_value_accordion( Value $value, array $ancestor_ids, $checked = false){   
        $category_id = $ancestor_ids['category_id'];
        $index = $ancestor_ids['category_index'];
        $subcategory_id = $ancestor_ids['subcategory_id'];
        $ancestor_ids['value_id'] = $value->id;
        $concern_checkboxes = '';
        foreach( $value->_concerns as $concern_id=>$concern ){
            if( is_object($concern))
            $concern_checkboxes .= $this->get_concern_element( 
                $concern, 
                $ancestor_ids, 
                is_array($value->_concern_ids) ? in_array($concern_id, $value->_concern_ids) : false
            );
        }
        $concern_checkboxes .= $this->get_new_element_div(self::TYPE_CONCERN,$ancestor_ids);  
        $value_accordion = $this->get_bootstrap_accordion(
            $this->get_DOM_id(self::TYPE_VALUE, $category_id, $index, $subcategory_id, $value->id),
            array(
                array(
                    'before_text'=> '<input type="checkbox" '.($checked ? 'checked="checked"' : '').' id="value_'.$category_id.'_'.$index.'_'.$subcategory_id.'_'.$value->id.'">',
                    'href'=> 'val_acc_'.$category_id.'_'.$index.'_'.$subcategory_id.'_'.$value->id,
                    'text' => $value->name,
                    'after_text' => '<a class="remove_element pull-right" '.$this->creata_remove_element_data_string(self::TYPE_VALUE, $ancestor_ids).'></a>',
                    'accordion_data' => $concern_checkboxes
                )
            )                    
        );
        return $value_accordion;    
    }

    public function get_subcategory_accordion( Subcategory $subcategory, array $ancestor_ids ){
        $category_id = $ancestor_ids['category_id'];
        $index = $ancestor_ids['category_index'];
        $subcategory_id = $subcategory->id;
        $ancestor_ids['subcategory_id'] = $subcategory_id;
        
        $value_accordions = '';
        foreach($subcategory->_values as $value_id => $value ){
            $value_accordions .= $this->get_value_accordion( 
                $value,
                $ancestor_ids,
                is_array($subcategory->_value_ids)? in_array($value_id, $subcategory->_value_ids) : false
            );
        }
        $value_accordions .= $this->get_new_element_div(self::TYPE_VALUE, $ancestor_ids);
        $subcategory_accordion = $this->get_bootstrap_accordion(
            $this->get_DOM_id(self::TYPE_SUBCATEGORY, $category_id, $index, $subcategory_id),
            array(
                array(                            
                    'heading_class' => 'accordion_heading_subcat',
                    'href'=> 'sub_acc_'.$category_id.'_'.$index.'_'.$subcategory_id,
                    'before_text'=> '<input type="checkbox" checked="checked" id="subcat_'.$category_id.'_'.$index.'_'.$subcategory_id.'">',
                    'after_text' => '<a class="remove_element pull-right" '.$this->creata_remove_element_data_string(self::TYPE_SUBCATEGORY, $ancestor_ids).'></a>',
                    'text' => $subcategory->name,
                    'accordion_data' => $value_accordions
                )                    
            )    
        );   
        
        return $subcategory_accordion; 
    }
    
    public function get_category_accordion( Category $category ){
       $ancestor_ids = array(
            'category_id' => $category->id,
            'category_index' => $category->_index,       
       );
       $subcategory_accordions = '';
       if( $gd_subcategory = $category->get_general_description_subcategory()){
            $subcategory_accordions .= $this->get_subcategory_accordion(
                $gd_subcategory,
                $ancestor_ids
            );
            $category->remove_general_description_subcategory_from_tree($category->_index, $gd_subcategory);   
        }
        
        foreach( $category->_subcategories as $subcategory_id=>$subcategory ){
            $subcategory_accordions .= $this->get_subcategory_accordion(
                $subcategory,
                $ancestor_ids
            );
        }
        $subcategory_accordions .= $this->get_new_element_div(self::TYPE_SUBCATEGORY, $ancestor_ids);
        $category_accordion = $this->get_bootstrap_accordion(
            $this->get_DOM_id(self::TYPE_CATEGORY, $category->id, $category->_index),
            array(
                array(
                    'heading_class' => 'accordion_heading_cat',
                    'href'=> 'cat_acc_'.$category->id.'_'.$category->_index,
                    'before_text' => '<input type="checkbox" checked="checked" id="cat_'.$category->id.'_'.$category->_index.'">',
                    'text' => $category->name,
                    'after_text' => '<a class="remove_element pull-right" '.$this->creata_remove_element_data_string(self::TYPE_CATEGORY, $ancestor_ids).'></a>',
                    'accordion_data' => $subcategory_accordions
                )                
            )
        );        
        return $category_accordion;                
    }
    
    public function get_DOM_id( $type, $category_id, $category_index, $subcategory_id=null, $value_id=null, $concern_id=null){
        switch($type){
            case self::TYPE_CATEGORY:
                $id = 'category_accordion_'.$category_id.'_'.$category_index;
            break;
            break;
            case self::TYPE_SUBCATEGORY:
                $id = 'subcategory_accordion_'.$subcategory_id;
            break;
            case self::TYPE_VALUE:
                $id = 'value_accordion_'.$value_id;
            break;                        
            case self::TYPE_CONCERN:
            break;
        }
        return $id;
    }
    public function get_category_accordions(Categories $categories ){
        $category_accordions = '';
        foreach( $categories->categories as $category_id=>$template_categories ){
            foreach( $template_categories  as $category_index=>$category ){
                $subcategory_accordions = '';
                if( $gd_subcategory = $category->get_general_description_subcategory()){
                    $subcategory_accordions .= $this->get_subcategory_accordion(
                        $gd_subcategory,
                        array(
                            'category_id' => $category_id,
                            'category_index' => $category->_index,
                        )
                    );                                           
                }
                foreach( $category->_subcategories as $subcategory_id=>$subcategory ){
                    if( isset($gd_subcategory) && $gd_subcategory->id == $subcategory->id) { continue; }
                    $subcategory_accordions .= $this->get_subcategory_accordion(
                        $subcategory,
                        array(
                            'category_id' => $category_id,
                            'category_index' => $category->_index,
                        )
                    );
                }
                $category_accordions .= $this->get_category_accordion(
                    $category            
                );
            }
        }
        return $category_accordions;        
    }
    
    public function get_new_element_div( $type, $ancestor_ids ){
        switch($type){
            case self::TYPE_CONCERN:
                $caption = 'New Concern';
            break;
            case self::TYPE_SUBCATEGORY:
                $caption = 'New Section';
            break;
            case self::TYPE_VALUE:
                $caption = 'New Description';
            break;
            default:
                $caption = 'New';
        }
        $div = 
        '<div class="div_concern">
             <span class="add_description form-inline">
                <input type="text" value="" name="new_description">
                <a '.$this->create_data_string($type, $ancestor_ids).'class="btn">Ok</a>
             </span>
             <a class="btn btn_add_desc">'.$caption.'</a>
         </div>';
        return $div;        
    }
    
    public function get_concern_element($concern, $ancestor_ids, $checked = false ){
        $category_id = $ancestor_ids['category_id'];
        $index = $ancestor_ids['category_index'];
        $subcategory_id = $ancestor_ids['subcategory_id'];
        $value_id = $ancestor_ids['value_id'];
        $ancestor_ids['concern_id'] = $concern->id;
        return '<div class="div_concern">
                    <div>
                        <label class="checkbox pull-left"><input type="checkbox" name="categories['.$category_id.']['.$index.']['.$subcategory_id.'][value_ids]['.$value_id.'][concern_ids]['.$concern->id.']" '.($checked ? 'checked="checked"' : '').' id="concern_'.$concern->id.'">'.$concern->name.'</label>
                        <a class="remove_element pull-right" '.$this->creata_remove_element_data_string(self::TYPE_CONCERN, $ancestor_ids).'></a>
                    </div>
               </div>';        
    }
    
    protected function create_data_string( $type, $ancestor_ids ){
        $array['data-category-id'] = $ancestor_ids['category_id'];
        $array['data-category-index'] = $ancestor_ids['category_index'];
        $array['data-subcategory-id'] = isset($ancestor_ids['subcategory_id']) ? $ancestor_ids['subcategory_id']: self::DEFAULT_NULL;
        $array['data-value-id'] = isset($ancestor_ids['value_id']) ? $ancestor_ids['value_id'] : self::DEFAULT_NULL;
        $array['data-concern-id'] = isset($ancestor_ids['concern_id']) ? $ancestor_ids['concern_id'] : self::DEFAULT_NULL;
        $array['data-type'] = $type;
        
        foreach( $array as $key=>$value){
            $string_array[] = $key.'="'.$value.'"';
        }
        return implode(' ', $string_array);
            
    }
    
    protected function creata_remove_element_data_string( $type, $ancestor_ids ){
        return $this->create_data_string($type, $ancestor_ids).' data-delete="true" ';
    }
    
    
}
