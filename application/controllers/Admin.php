<?php
class Admin extends MY_Controller{
    
    const AJAX_CLIENT_ORDERS = 'client_orders';

    private $field_list = array(
            'id',
            'status_string::name',
            'term::name',
            'agent::name',
            'inspector::name',
            'inspection::name',
            'meet_with',
            'address',
            'city',
            'active',
            'client::phone_1',
            'total',
            'paid',
            'amount_due'
    );
    
    public function __construct(){  
        parent::__construct();
        //This section is only for Admins and Users
        $this->authorize_or_redirect( array(Person::TYPE_ADMIN));
        $this->load->library('authorize_payment');           
    }
    public function index(){
         
        $company_filter = new Company();
        $companies = null;
        $users = array();
        $selected_company = null;
        $new_company = null;
        $new_user = null;
        $saved = null;
        $company_id = null;
        
        if( $this->input->post('new_company') ){
            $new_company = new Company();
        } else if( $this->input->post('save_company')){ 
            unset($_POST['company_id']);
            $company = $this->order_mapper->create_object_from_input('Company');
            if( $company->validate()){
                $id = $this->order_model->save_or_update_object( $company );
                if( $id == $company_id ){
                    $saved = 'update';
                } else {
                    $saved = true;
                }
                $company_id = $id;
            } else {
                $saved = false;
            }
        }  
        /**
        * IF company is selected - Load it
        */
        if( $company_id !== null || ($company_id = $this->input->post('company_id')) ){
            
            $person_filter = new Person();
            $person_filter->set_company_id( $company_id );            
            $users = $this->order_model->get_object( $person_filter );
            
            $companies = $this->order_model->get_object( $company_filter );
            foreach( $companies as $company ){
                if( $company->id == $company_id ){
                    $selected_company = $company;
                    break;
                }
            }            
        }
        /**
        * Load all companies ( For the side div select box)
        */
        if( $companies == null ){
            $companies = $this->order_model->get_object( $company_filter );    
        }
        /**
        * Company select box statuses
        */
        $statuses = array(
            1 => 'Active',
            0 => 'Disabled'        
        );
        $companies =  $companies; 
		
		$headermenuitems = $this->order_model->get_menuitems();
		
        /**
        * Load views
        */
        $this->load->view('common/header', array(   'fluid_layout'=>false,'user'=>$this->user,   'companies'=>$this->companies,'headermenuitems'=>$headermenuitems ));
        $this->load->view('admin/admin', array(     'companies' => $companies ,
                                                    'users'     => $users,                                                    
                                                    'statuses' => $statuses,                                                    
                                                    'saved'    => $saved,
                                                    'states'   => $this->order_model->get_us_states(),                                                    
                                                    'selected_company' => $selected_company ));
        $this->load->view('common/footer');        
    }
    
    public function ajax_control(){ 
        if( ($company_id = $this->input->post('company_id', TRUE))){                 
            $this->session->set_userdata('company_id', $company_id);
        }
    }
    public function add_new(){
         
        /**
        * Type of new object
        */
        $class_name = $this->uri->segment(3,0);
        /**
        * Company id (Only for new USER)
        */
        $company_id = $this->uri->segment(4,0);
        /**
        * Show user -> click on Coordinators table entry
        * 
        * 
        */
        $show_user = $this->uri->segment(5,0);
        
        $date = array();
        if( $this->input->post('save') && in_array( $class_name, array('Person','Company') ) ){
            //object can be one of 2 types (person, company)

            $object = $this->order_mapper->create_object_from_input($class_name);            
            
            if( $object->validate()){
                $old_id = $object->id;
                $object->set_id($this->order_model->save_or_update_object( $object ));
                if( $old_id != $object->id ) {
                    
                    $data = array(
                        'name' => 'Completed' ,
                        'company_id' => $object->id ,
                        'active' => '1'
                     );
                     $this->db->insert('jobstatus', $data); 
                    
                    $saved = true;    
                } else {
                    $saved = 'update';
                }
            } else {
                $saved = false;
            } 
            $data  = array('object' => $object, 'saved' => $saved, 'action' => site_url('admin/add_new/'.$class_name));                 
            
        }  else {
            /**
            * User clicked ADD NEW 
            */
            if( $show_user ){
              $filter = new Person();
              //company_id is user_id in this instance
              $filter->set_id( $company_id );
              $users = $this->order_model->get_object( $filter );
              $user = new Person();
              if( $users ){
                  $user = reset($users);
              }
              $data = array( 'object' => $user, 'action' => site_url('admin/add_new/'. $class_name) );
            } else if( in_array( $class_name, array('Person','Company'))){
                $object = new $class_name();
                if( $class_name == 'Person'){
                    $object->set_company_id( $company_id );
                    $object->set_person_type( Person::TYPE_USER );
                }
                $data = array( 'object' => $object, 'action' => site_url('admin/add_new/'. $class_name) );                
            } 
        }
        
        $data['states'] = $this->order_model->get_us_states();
        $this->load->view('common/header', array('fluid_layout'=>true, 'user'=>$this->user,'dont_display_header'=>true));             
        $this->load->view('common/add_new', $data );             
        $this->load->view('common/footer');           
    }
    public function transactions(){   
        $transactions = $this->order_model->get_object(new Transaction());
        
        $this->load->view('common/header', array('fluid_layout'=>false, 'user'=>$this->user,'companies'=>$this->companies));             
        $this->load->view('admin/transactions', array('transactions'=>$transactions  ) );             
        $this->load->view('common/footer');         
        
    }
}
