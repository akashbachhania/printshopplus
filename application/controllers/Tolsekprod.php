<?php
class Tolsekprod extends MY_Controller{
    public function __construct(){ 
         parent::__construct();
        $this->authorize_or_redirect(); 
    }
    public function index(){
		$person = new Person();
        $person->set_order_by( array('name'));
        $person->set_company_id( $this->user->company_id );
        $this->load->view('common/header', array('fluid_layout'=>true,'user'=>$this->user, 'companies'=>$this->companies));
		$this->load->view('tolsekprod/tolsekprod');
		$this->load->view('common/footer');
    }
}