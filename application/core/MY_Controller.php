<?php
class MY_Controller extends CI_Controller{
    
    protected $user;
    protected $companies;
    public function __construct(){
        $this->companies = array();
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('html_table');
        //Main Model for this project. Contains DB communication and Record classes (Order,Person,Item, etc )
        $this->load->model('Order_model','order_model', TRUE );                

        
    }
    protected function authorize_or_redirect( $authorize_types = array()){  
        $id = $this->session->userdata('id');
        $person_type = $this->session->userdata('person_type');
        $company_id  = ($company_id =$this->session->userdata('company_id')) ? $company_id : 0;
        
        
        
        if( ($id == FALSE  && $person_type == FALSE) || 
             ($authorize_types && !in_array( $person_type, $authorize_types ))   ){              
             $this->redirect();
        } else {
 
            $filter  = new Person();
            $filter->set_id( $id );
            $filter->set_person_type( $person_type );
            
            $persons = $this->order_model->get_persons( $filter );
            
            if( $persons ){
                $person = reset( $persons );
                $this->user = $person;
                /**
                * Override company id for ADMIN (can see all companies)
                */
                if( $this->user->person_type == Person::TYPE_ADMIN ){
                    $this->user->set_company_id( $company_id );
                    
                    $this->companies = $this->order_model->get_companies( $this->user ) + array('0' => '-Select Company-');
                    
                }
                //Some helper libraries our view uses
                $this->load->library('table');
                //Used for Record form creation
                $this->load->library('order_helper');
                //Used for Mapping of Input to Record class
                $this->load->library('order_mapper', array('input'=>$this->input));                
            }  else {
                $this->redirect();
            }
        }
    }
    
    protected function get_local_path(){
        $path = parse_url( site_url()); 
        $path = str_replace('index.php','',$path['path']);        
        return $_SERVER['DOCUMENT_ROOT'].$path;
    }
 
    protected function log( $message = '', $file = '', $append = false ){ 
        $path = $this->get_local_path() .'application/logs/';
        $file = $file ? $file : 'log.txt';
        if( $message ){
            file_put_contents( $path . $file, '['.gmdate('Y-m-d h:i:s').'] '. $message ."\n", $append );     
        }
                       
    }
    
    protected function redirect(){
        header('Location: ' . site_url('login/'));
        die();        
    }
    
    protected function initialize_email(){
        $this->load->library('email'); 
        $config['mailtype']  = 'html';
        $config['protocol']  = 'smtp';
        $config['smtp_host'] = 's109311.gridserver.com';
        $config['smtp_user'] = 'divo@homeinspectorplus.com';
        $config['smtp_pass'] = 'divomail123';
        $this->email->initialize( $config );         
    }
    
    function image_resize( $f, $new_file, $type, $size = null ){
         
        $imgFunc = '';
        $img_data = getimagesize($f);
        switch($type)
        {
            case 'image/gif':
                $img = ImageCreateFromGIF($f);
                $imgFunc = 'ImageGIF';
                $transparent_index = ImageColorTransparent($img);
                if($transparent_index!=(-1)) $transparent_color = ImageColorsForIndex($img,$transparent_index);
                break;
            case 'image/jpeg':
                $img = ImageCreateFromJPEG($f);
                $imgFunc = 'ImageJPEG';
                break;
            case 'image/png':
                $img = ImageCreateFromPNG($f);
                ImageAlphaBlending($img,true);
                ImageSaveAlpha($img,true);
                $imgFunc = 'ImagePNG';
                break;
        }
 
 
        if( $size ){
            list($w,$h) = GetImageSize($f);
             
            $percent = $size / (($w>$h)?$w:$h);
            if($percent>0 and $percent<1)
            {
                $nw = intval($w*$percent);
                $nh = intval($h*$percent);
                $img_resized = ImageCreateTrueColor($nw,$nh);
                if($type=='image/png')
                {
                    ImageAlphaBlending($img_resized,false);
                    ImageSaveAlpha($img_resized,true);
                }
                if(!empty($transparent_color))
                {
                    $transparent_new = ImageColorAllocate($img_resized,$transparent_color['red'],$transparent_color['green'],$transparent_color['blue']);
                    $transparent_new_index = ImageColorTransparent($img_resized,$transparent_new);
                    ImageFill($img_resized, 0,0, $transparent_new_index);
                }
                if(ImageCopyResized($img_resized,$img, 0,0,0,0, $nw,$nh, $w,$h ))
                {
                    ImageDestroy($img);
                    $img = $img_resized;
                   
                }
            }            
        }

        $imgFunc($img, $new_file);
        ImageDestroy($img);         
    }    
    
   
}
