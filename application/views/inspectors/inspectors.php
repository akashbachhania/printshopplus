<?php  
if( $saved !== null){
    if( $saved === true ){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>Your inspector has been saved';                    
    } else if ( $saved === false ) {
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your update has not been saved (Please correct fields with red border)';            
    } else if ( $saved === 'update') {
        $saved = 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your inspector has been updated successfully!';             
    } else if ( $saved == 'booked'){
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Inspector already has booked inspection for given timoff period!';        
    }
} else {
    $saved = '';                         
    $msg  = '';
}
/**
* We'll override names
*/
$overrides = array(
    'status_string::name' => 'Status',
    'term::name' => 'Terms',
    'agent::name' => 'Agent',
    'inspector::name' => 'Sales Rep',
    'inspection::name' => 'Inspection',
    'client::phone_1' => 'Phone',
    'company_order_id' => 'ID'
);

$fields = array(
   'name','address','city','state','zip','phone_1','phone_2','email', 'password','timeoff_date_start','timeoff_time_start','timeoff_date_end','timeoff_time_end', 'active','id','company_id','person_type'
);
if( $orders && is_object( $orders['0'] ) && isset( $orders['0']->client )){
    $client = $orders['0']->client;    
}
$options = array(
    1 => 'Active Sales Rep',
    0 => 'Disabled Sales Rep'
)  
?>
            <div class="row div_header">
                
                <div id="div_inspectors_table" class="span4">
                    <div class="well">
                        <legend>Sales Rep</legend>
                        <div name="controls">                                    
                        <?php echo $this->order_helper->generate_table( $inspectors, array(
                                    'id',
                                    'name'
                                    ), 
                                    $this->table);
                        ?>
                        <a id="btn_new_person" href="<?php echo site_url('inspectors/add_new/Person')?>" target="__blank" class="btn">Add new</a>
                        </div>
                    </div>
                </div>                                          
                <div id="div_orders_table" class="span7">
                    <?php if( $saved ){ ?>
                        <div id="div_order_added" class="<?php echo $saved ?>">
                                <a class="close" data-dismiss="alert">�</a>
                                <?php echo $msg ?>
                        </div>
                    <?php } ?>                    
                    <?php 
                        if( $selected_inspector ){
                            $times = array(''=>'') + $times;  
                            echo $this->order_helper->get_object_form( $selected_inspector, $fields, $this->table, 
                                array(
                                    'active' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $options,
                                        'selected' => $selected_inspector->active ),
                                    'timeoff_date_start' => array(
                                        'date' => true,
                                        'inline' => 'timeoff_time_start',
                                        'class'  => 'input-small'
                                    ),
                                    'timeoff_time_start' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $times,
                                        'selected' => $selected_inspector->timeoff_time_start,
                                        'class'    => 'input-small inline'  ),
                                    'state'=> array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                        'options'=>  $states,
                                        'selected'=> $selected_inspector->state ),                                                                            
                                    'timeoff_date_end' => array(
                                        'date' => true,
                                        'inline' => 'timeoff_time_end',
                                        'class'  => 'input-small'                                        
                                    ),
                                    'timeoff_time_end' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $times,
                                        'selected' => $selected_inspector->timeoff_time_end,
                                        'class'  => 'input-small inline' ),
                                                                                                                                     
                                    '__legend' => 'Sales Rep details: '.$selected_inspector->name,
                                    '__form'   => array(
                                        'action' => site_url('inspectors'), 
                                        'submit_value' => 'Save',
                                        'submit_name'  => 'save_inspector'
                                        )
                                    )
                            );   
                        }
                    unset($order_fields['id']);
                    $order_fields = array('company_order_id') + $order_fields;
                    ?>
                    <div class="well"> 
                        <legend><?php echo ($selected_inspector) ? 'Orders for '.$selected_inspector->name : 'Sales Rep Orders'; ?></legend>
                        <div name="controls" id="div_inspector_details">                     
                        <?php echo $this->order_helper->get_order_table( $orders, $order_fields, $this->html_table, $overrides );?>
                        </div>
                    </div>
                </div> 
            </div>