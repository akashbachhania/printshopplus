<?php
	$orderid=$this->uri->segment(3);	
	$posturl= isset($orderid)? site_url('orders/save_runorders/'.$orderid) : site_url('orders/save_runorders');
?>
<form id="frmrunorder" action="<?echo $posturl;?>" method="post" enctype="multipart/form-data">
<input type="hidden" id="oid" name="oid" value="<?echo $_GET["oid"];?>" />
<input type="hidden" id="jid" name="jid" value="<?echo $_GET["jid"];?>" />
<div align="center">
<img src="<?php echo site_url('/application/views/assets/img/formsetup.jpg');?>" width="100px" height="130px" border="1" />
</div>
	<div class="row">
		<div id="div_order_details" class="well">
			<legend>Run #<?php echo $max_run_id;?></legend>
			<input type="hidden" id="hdn_max_run_id" name="hdn_max_run_id" value="<?php echo $max_run_id;?>" />
			<table width="100%">						  
			  <tr>
				<td>Size</td>
				<td><input type="text" name="Size" id="Size" size="30" value=""></td>
				<td>Paperstock</td>
				<td><input type="text" name="PaperStock" id="PaperStock" size="30" value=""></td>    
			  </tr>
			  <tr>
				<td>Colors</td>
				<td><input type="text" name="Colors" id="Colors" size="30" value=""></td>
				<td>Quantity</td>
				<td><input type="text" name="Quantity" size="30" value="" id="Quantity"></td>    
			  </tr>
			  <tr>
				<td>Coating</td>
				<td>
					<select onchange="ChangeCoating();" id="Coating" name="Coating" width="100">
					<?php
					foreach($coatlist as $items ){
						echo '<option value="'. $items.'">'. $items.'</option>';
					}  
					?>
					</select>
				</td>
				<td>Due Date</td>
				<td><input id="due_date" class="input-small" type="text" value="" name="due_date" data-datepicker="datepicker"></input></td>    
			  </tr>  
			  <tr>
				<td>Proofs</td>
				<td><input type="text" name="Proofs" size="30" value=""></td>
				<td></td>
				<td></td>    
			  </tr>
			  <tr>
				<td>Additional Info/Comments</td>
				<td colspan="3">
					<textarea name="AdditionalInfo" id="AdditionalInfo" rows="5" cols="60" style="width:100%" value=""></textarea>
				</td>
			  </tr>
			  <tr>
				<td>Upload Image</td>
				<td>
					<input type="file" id="photo_file" name="photo_file" size="50">
				</td>
			  </tr>
			  <tr>
				<td colspan="4" align="center">Job Listing</td>
			  </tr>
			  <tr>
				<td colspan="4" align="center">
					<div id="div_orders_table" class="well">
                    <legend>Orders</legend>
                    <?php echo $this->order_helper->get_order_tables( $orders, $statuslist, $operatorlist, array(
								'id',
								'edit',
								'client_id',
                                'order_date',								
								'order_notes',
								'report_due',
								'sales_rep',
								'status',
								'Operator'
                          ), 
                          $this->html_table,
                          $overrides);
                    ?>
                </div>
				</td>
			  </tr> 
			  <tr>
				<td colspan="4" align="center">
					<div id="process"></div>
				</td>
			  </tr>
			  <tr>
				<td colspan="4" align="center">
					<input type="hidden" name="printRunId" value="7">
					<!-- <a href="<?php echo site_url('orders/generate_run_pdf/'.$max_run_id);?>" target="_blank"> -->
					
					<input type="submit" value="Next" onclick="return validate_form();" />
				</td>
			  </tr>			  
			</table>		
		</div>
	</div> 									
</form>
<script type="text/javascript">
function validate_form(){
        validator= jQuery('#frmrunorder').validate({
            rules:{
                'Size':{
                    required: true,
                },
				'PaperStock':{
                    required: true,
                },
				'Quantity':{
				    required: true,
                    number:true
				},
				'Coating':{
                    required: true,
                },
				'due_date':{
                    required: true,
                },
				'Proofs':{
				    required: true,
				},
				'photo_file':{
				    required: true,
				}
            },
            messages:{
				 'Size':{
				 	 required: "<br />This field cannot be blank"
				 },
				 'PaperStock':{
				 	 required: "<br />This field cannot be blank"
				 },
				 'Quantity':{
				 	 required: "<br />This field cannot be blank",
					 number:"<br />Please enter a valid number"
				 },
                 'Coating':{
                      required: "<br />This field cannot be blank"
                },
				'due_date':{
                      required: "<br />This field cannot be blank"
                },
				'Proofs':{
                      required: "<br />This field cannot be blank"
                },
				'photo_file':{
                      required: "<br />This field cannot be blank"
                }
            }
        });
       x= validator.form();
		if(x){
			return true;
		}else{
			return false;
		}
    }
	
	function Generate_Temp_Run_Order()
	{
		if(validate_form()){
			var formData = new FormData($("#frmrunorder")[0]);
			formData.append("oid",<?php echo $_GET["oid"]?>);
			formData.append("runid",$('#hdn_max_run_id').val());
			$.ajax({
				type: "POST",
				url: "http://"+$(location).attr('hostname')+"/HIP/orders/save_runorders_temp",
				data: formData,
				async: false,
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() 
				{
					$("#process").html('<div style="padding-left:100px;margin-bottom:30px;"><font style="font-family:Verdana, Geneva, sans-serif; font-size:12px; color:black;">Please wait</font> <img src="http://'+$(location).attr('hostname')+'/HIP/application/views/assets/img/vpsloader/loading.gif" align="absmiddle" /></div>');				
				},
				success: function(response)
				{			
					$("#process").html('');
					var ret_val  = jQuery.parseJSON(response);
					 //if( ret_val['status'] == 'success' ){
						window.open("<?php echo site_url('orders/generate_run_pdf_temp/')?>/"+ret_val['auto_id']);
					//} 
				}
			});
		}
	}
</script>