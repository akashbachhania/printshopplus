<?php
if( $saved !== null){
    if( $saved === true ){
        $saved = 'alert alert-success';
        $msg   = '<strong>Success: </strong>Your order has been saved';                    
    } else if ( $saved === false ) {
        $saved = 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your order has not been saved (Please correct fields with red border)';            
    } else if ( $saved === 'update') {
        $saved = 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your order has been updated successfully!';             
    }
} else {
    $saved = '';                         
    $msg  = '';
}
/**
* We'll override names
*/
$overrides = array(
    'status_string::name' => 'Status',
    'term::name' => 'Terms',
    'agent::name' => 'Agent',
    'inspector::name' => 'Sales Rep',
    'inspection::name' => 'Inspection',
    'client::phone_1' => 'Phone',
    'company_order_id' => 'ID'
);
 
if( $orders && is_object( $orders['0'] ) && isset( $orders['0']->client )){
    $client = $orders['0']->client;    
}
$fields = array('name','contact' ,'address','city','state','zip','phone_1','phone_2','email','email_2','email_3', 'active','notes', 'id','company_id','person_type');
// $fields = array('name','contact' ,'address','city','state','zip','phone_1','phone_2','email','email_2','email_3', 'active','id','company_id','person_type');
$options = array(
    1 => 'Active Client',
    0 => 'Disabled Client'
)  
?>
            <div class="row div_header">
                
                <div id="div_clients_table" class="span4">
                    <div class="well">
                        <legend>Clients</legend>
                        <div name="controls">                                    
                        <?php   echo $this->order_helper->get_clients_table( $clients, array(
                                    'id',
                                    'name',
                                    'amount_due',
                                    ), 
                                    $this->table);
                        ?>
                        <a id="btn_new_person" href="<?php echo site_url('clients/add_new/Person')?>" target="__blank" class="btn">Add new</a>
                        </div>
                    </div>
                    <div>
                        <a id="a_excel"   data-placement="top" rel="tooltip"  data-original-title="Export contacts to Excel" href="<?php echo site_url('clients/export')?>" target="_blank"></a>
                    </div>                    
                </div>                                          
                <div id="div_orders_table" class="span8">
                    
                    <?php if( $saved ){ ?>
                        <div id="div_order_added" class="<?php echo $saved ?>">
                                <a class="close" data-dismiss="alert">x</a>
                                <?php echo $msg ?>
                        </div>
                    <?php } ?>                    
                    <?php 
                        if( $selected_client ){
                             
                            echo $this->order_helper->get_object_form( $selected_client, $fields, $this->table, 
                                array(
                                    'notes' => array('element'=> Order_helper::ELEMENT_TEXTAREA),
                                    'active' => array( 
                                        'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                        'options'  => $options,
                                        'selected' => $selected_client->active ),
                                    'state'=> array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                        'options'=>  $states,
                                        'selected'=> $selected_client->state ),    
                                    
                                    '__legend' => 'Client details: '.$selected_client->name,
                                    '__form'   => array(
                                        'action' => site_url('clients'), 
                                        'submit_value' => 'Save',
                                        'submit_name'  => 'save_client'
                                        )
                                    )
                            );
  
                        }
                    
                        unset($order_fields['id']);
                        $order_fields = array('company_order_id') + $order_fields;
                        
                    ?>

                        <div class="well">
                            <legend>Client orders</legend>
                            <div name="controls" id="div_client_details">                     
                            <?php // $this->order_helper->get_order_table( $orders, $order_fields, $this->html_table, $overrides );?>
                                <table>
                                    <thead>
                                        <th>Invoice #</th>
                                        <th>Job name</th>
                                        <th>Sales Rep</th>
                                        <th>Address</th>
                                        <th>City</th>
                                        <th>Paid</th>
                                        <th>Amount due</th>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if($orders) {
                                            
                                            foreach ($orders as $order) {
                                                $getJobDetail = $this->order_model->orderJobDetails($order->id);
                                                $getShirtDetail = $this->order_model->orderShirtJobDetails($order->id);
                                                if($getJobDetail) {
                                                    foreach($getJobDetail as $job) {
                                                        $link = '<a href="'.base_url().'orders/show_order/'.$order->id.'">%s</a>';
                                                ?>
                                                    <tr>
                                                        <td><?php echo sprintf($link, $order->company_order_id);?></td>
                                                        <td><?php echo sprintf($link, $job->job_name);?></td>
                                                        <td><?php echo sprintf($link, (isset($order->inspector->name)) ? $order->inspector->name : '');?></td>
                                                        <td><?php echo sprintf($link, $order->address);?></td>
                                                        <td><?php echo sprintf($link, $order->city);?></td>
                                                        <td><?php echo sprintf($link, $order->paid);?></td>
                                                        <td><?php echo sprintf($link, $order->amount_due);?></td>
                                                    </tr>
                                                <?php
                                                    }
                                                }
                                                
                                                if($getShirtDetail) {
                                                    foreach($getShirtDetail as $job) {
                                                        $link = '<a href="'.base_url().'orders/show_order/'.$order->id.'">%s</a>';
                                                ?>
                                                    <tr>
                                                        <td><?php echo sprintf($link, $order->company_order_id);?></td>
                                                        <td><?php echo sprintf($link, $job->job_name);?></td>
                                                        <td><?php echo sprintf($link, (isset($order->inspector->name)) ? $order->inspector->name : '');?></td>
                                                        <td><?php echo sprintf($link, $order->address);?></td>
                                                        <td><?php echo sprintf($link, $order->city);?></td>
                                                        <td><?php echo sprintf($link, $order->paid);?></td>
                                                        <td><?php echo sprintf($link, $order->amount_due);?></td>
                                                    </tr>
                                                <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    
                </div> 
            </div>
<style>
    #div_orders_table .table tr td:nth-child(2), #div_orders_table .table tr th:nth-child(2) {
        display: block!important;
    }
    #div_client_details .dataTables_info {
        margin-left: 29px;
        width: 200px;
    }
</style>