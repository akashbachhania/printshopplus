<?php
$overrides = array(
    'report_due' => 'Due date',
    'sales_rep' => 'Sales Rep',
    'order_notes' => 'Description',
    'client_id' => 'Order #',
    'company_id' => 'Company'
);

$source = $this->uri->segment(4);
?>
<section class="wrapper retracted scrollable">
   <script>
        if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
            if ($.cookie('protonSidebar') == 'retracted') {
                $('.wrapper').removeClass('retracted').addClass('extended');
            }
            if ($.cookie('protonSidebar') == 'extended') {
                $('.wrapper').removeClass('extended').addClass('retracted');
            }
        }
    </script>
           
    <nav class="user-menu">
        <a href="javascript:;" class="main-menu-access">
            <i class="icon-proton-logo"></i>
            <i class="icon-reorder"></i>
        </a>

        <div class="panel panel-default nav-view messages-view">
            <div class="arrow user-menu-arrow"></div>
            <div class="panel-heading">
                <i class="icon-envelope-alt"></i>
                <span>Messages</span>
                <a href="javascript:;" class="close-user-menu"><i class="icon-remove"></i></a>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <i><img src="<?php echo base_url()?>assets/images/user-icons/user1.jpg" alt="User Icon"></i>
                    <div class="text-holder">
                        <span class="title-text">
                            Nunc Cenenatis:
                        </span>
                        <span class="description-text">
                            Hi, can you meet me at the office tomorrow morning?
                        </span>
                    </div>
                    <span class="time-ago">
                        3 mins ago
                    </span>
                </li>
                <li class="list-group-item">
                    <i><img src="<?php echo base_url()?>assets/images/user-icons/user5.jpg" alt="User Icon"></i>
                    <div class="text-holder">
                        <span class="title-text">
                            Prasent Neque:
                        </span>
                        <span class="description-text">
                            Just a quick question: do you know the balance on the adsense account?
                        </span>
                    </div>
                    <span class="time-ago">
                        17 mins ago
                    </span>
                </li>
                <li class="list-group-item">
                    <i><img src="<?php echo base_url()?>assets/images/user-icons/user2.jpg" alt="User Icon"></i>
                    <div class="text-holder">
                        <span class="title-text">
                            Flor Demoa:
                        </span>
                        <span class="description-text">
                            Hey, we're going surfing tomorrow. Feel free to join in.
                        </span>
                    </div>
                    <span class="time-ago">
                        3 hrs ago
                    </span>
                </li>
            </ul>
        </div>

        <div class="panel panel-default nav-view notifications-view">
            <div class="arrow user-menu-arrow"></div>
            <div class="panel-heading">
                <i class="icon-comment-alt"></i>
                <span>Notifications</span>
                <a href="javascript:;" class="close-user-menu"><i class="icon-remove"></i></a>
            </div>
            <ul class="list-group">
                <li class="list-group-item">
                    <i><img src="<?php echo base_url()?>assets/images/user-icons/user1.jpg" alt="User Icon"></i>
                    <div class="text-holder">
                        <span class="title-text">
                            Nunc Cenenatis
                        </span>
                        <span class="description-text">
                            likes your website.
                        </span>
                    </div>
                    <span class="time-ago">
                        32 mins ago
                    </span>
                </li>
                <li class="list-group-item">
                    <i><img src="<?php echo base_url()?>assets/images/user-icons/user2.jpg" alt="User Icon"></i>
                    <div class="text-holder">
                        <span class="title-text">
                            Flor Demoa
                        </span>
                        <span class="description-text">
                            wrote a new post.
                        </span>
                    </div>
                    <span class="time-ago">
                        3 hrs ago
                    </span>
                </li>
                <li class="list-group-item">
                    <i><img src="<?php echo base_url()?>assets/images/user-icons/user4.jpg" alt="User Icon"></i>
                    <div class="text-holder">
                        <span class="title-text">
                            Nunc Neque
                        </span>
                        <span class="description-text">
                            wrote a new post.
                        </span>
                    </div>
                    <span class="time-ago">
                        57 mins ago
                    </span>
                </li>
                <li class="list-group-item">
                    <i><img src="<?php echo base_url()?>assets/images/user-icons/user2.jpg" alt="User Icon"></i>
                    <div class="text-holder">
                        <span class="title-text">
                            Flor Demoa
                        </span>
                        <span class="description-text">
                            submitted a new ticket.
                        </span>
                    </div>
                    <span class="time-ago">
                        1.5 hrs ago
                    </span>
                </li>
                <li class="list-group-item">
                    <i><img src="<?php echo base_url()?>assets/images/user-icons/user1.jpg" alt="User Icon"></i>
                    <div class="text-holder">
                        <span class="title-text">
                            Nunc Cenenatis
                        </span>
                        <span class="description-text">
                            wrote a new post.
                        </span>
                    </div>
                    <span class="time-ago">
                        3 hrs ago
                    </span>
                </li>
            </ul>
        </div>
    </nav>
    
    <ol class="breadcrumb breadcrumb-nav">
        <li><a href="."><i class="icon-home"></i></a></li>
        <li class="active">
            <a class="bread-page-title" data-toggle="dropdown" href="#">View All</a>
            <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
            </ul>
        </li>
    </ol>
    <div id="top" class="panel panel-default panel-block panel-title-block">
        <div id="div_second_members"  class="span12">    
            <form id="form_total_statistics" method="post" action="">
                <div class="span12">
                    <div id="div_statistics_total">
                        <table class="" id="statistics_table">
                            <tr>
                                <td style="width: 30%;padding:20px">
                                    <div class="form form-inline" id="div_form_total_visits">
                                        <input name="set_date" type="submit" class="btn btn-info" value="Set"/>
                                        <input name="start_date" class="input-small" type="text" id="start_date" data-datepicker="datepicker" value="<?php echo $start_date ?>" style="width:35%;"/>-<input  class="input-small" id="end_date" name="end_date" type="text" data-datepicker="datepicker" value="<?php echo $end_date ?>"  style="width:35%;"/>
                                        <input name="check_feed" class="input-small" type="hidden" id="check_feed" value="<?php echo strtotime($end_date) < strtotime(gmdate('m-d-Y')) ? 'once' : 'true' ?>"/>
                                    </div> 
                                </td>
                                <td>
                                    <table id="show_statistics">
                                        <tr>
                                            <td><b>Total Jobs:</b> <?php echo $allJobs?></td>
                                            <td style="background-color: transparent!important;"><b>Active Jobs:</b> <?php echo $activeJobs?></td>
                                            <td><b>Completed jobs:</b> <?php echo $completedJobs;?></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>    
                </div>
            </form>
        </div>
    </div>       
            
    <div class="row">
        <div class="col-md-12">
            
            <div class="panel panel-default panel-block" id="div_orders_listing">
                <div id="data-table" class="panel-heading datatable-heading">
                    <h4 class="section-title">Orders</h4>
                </div>

                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th style="width: 50px;"><img title="Delete" src="<?php echo base_url(); ?>application/views/assets/img/trash.png" name="deleteall" id="deleteall"></th>
                            <th style="width: 50px;">Edit</th>
                            <th style="width: 50px;">Order #</th>
                            <th style="width: 80px;">Order date</th>
                            <th style="width: 150px;">Company</th>
                            <th style="width: 280px;">Description</th>
                            <th>Due date</th>
                            <th>Sales Rep</th>
                            <th>Status</th>
                            <th>Operator</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
        </section>
