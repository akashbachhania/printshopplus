<section class="wrapper retracted scrollable">
    <script>
        if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
            if ($.cookie('protonSidebar') == 'retracted') {
                $('.wrapper').removeClass('retracted').addClass('extended');
            }
            if ($.cookie('protonSidebar') == 'extended') {
                $('.wrapper').removeClass('extended').addClass('retracted');
            }
        }
    </script>
           
    <nav class="user-menu">
        <a href="javascript:;" class="main-menu-access">
            <i class="icon-proton-logo"></i>
            <i class="icon-reorder"></i>
        </a>
    </nav>
    
    <ol class="breadcrumb breadcrumb-nav">
        <li><a href="."><i class="icon-home"></i></a></li>
        <li class="active">
            <a class="bread-page-title" data-toggle="dropdown" href="#"></a>
            <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
            </ul>
        </li>
    </ol>
    
    <div id="top" class="panel panel-default panel-block panel-title-block">
        <div class="panel-heading">
            <div>
                <i class="icon-edit"></i>
                <h1>
                    <span class="page-title"></span>
                    <small>
                        Lorem ipsum dolor sit amet, consectetur adipisicing elit...
                    </small>
                </h1>
            </div>
        </div>
    </div>       
            
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default panel-block">
                <div class="list-group">
                    <div class="list-group-item">
                                
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
