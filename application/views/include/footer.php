
        <script src="<?php echo base_url()?>assets/scripts/bootstrap.min.js"></script>

        <!-- Proton base scripts: -->
        
        <script src="<?php echo base_url()?>assets/scripts/main.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/proton/common.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/proton/main-nav.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/proton/user-nav.js"></script>

        <!-- Page-specific scripts: -->
        <script src="<?php echo base_url()?>assets/scripts/proton/sidebar.js"></script>
        <script src="<?php echo base_url()?>assets/scripts/proton/tables.js"></script>
        <!-- jsTree -->
        <script src="<?php echo base_url()?>assets/scripts/vendor/jquery.jstree.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
            <script src="<?php echo base_url()?>assets/scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="<?php echo base_url()?>assets/scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="<?php echo base_url()?>assets/scripts/vendor/select2.min.js"></script>

            <!--Custom Scripts-->
            <script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.fancybox.js?v=2.1.5' ?>" type="text/javascript"></script>
            <script src="<?php echo base_url() . 'application/views/assets/js/jquery.timepicker.js' ?>" type="text/javascript"></script>
        
      </body>
</html>
