<?php
$legend = 'Select type';
if( $objects ){
    switch( get_class( $objects['0'])){
        case 'Person':
            $legend   = 'Users';
            $fields   = array('id','name','address','person_type','active');
            $specials = array(
                                'person_type' => array(
                                    'values' => array(
                                        Person::TYPE_AGENT  => 'Agent',
                                        Person::TYPE_INSPECTOR  => 'Inspector',
                                        Person::TYPE_CLIENT  => 'Client',
                                        'default' => '<value>'
                                    )
                                ),
                                '__href' => array(
                                    'href' => site_url('types/add_new/show_object/Person/<id>'),
                                    'id' => '<id>',
                                    'target' => '__blank',
                                )
                            );                
                         
        break;
        case 'Type':
            $legend = 'Types';
            $fields = array('id','name','description', 'active');
            $specials = array(
                                '__href' => array(
                                    'href' => site_url('types/add_new/show_object/Type/<id>'),
                                    'id' => '<id>',
                                    'target' => '__blank',
                                )
                            );              
        break;
        case 'Base_item':
            $legend = 'Charges';
            $fields = array('id','name','description','price','active');
            $specials = array(
                                '__href' => array(
                                    'href' => site_url('types/add_new/show_object/Base_item/<id>'),
                                    'id' => '<id>',
                                    'target' => '__blank',
                                )
                            );             
        break;
        case 'Tax':
            $legend = 'Taxes';
            $fields = array('id','name','description','value','active');
            $specials = array(
                                '__href' => array(
                                    'href' => site_url('types/add_new/show_object/Tax/<id>'),
                                    'id' => '<id>',
                                    'target' => '__blank',
                                )
                            );                         
        break;
    }
}

?>
            <div class="row-fluid div_header">                     
                        <div class="span3 well">
                            <form id="form_types" class="form-horizontal" action="<?php echo site_url('types')?>" method="post" >
                                <fieldset>
                                    <legend>Types</legend>
                                    <div name="controls">
                                        <div class="control-group">
                                            <label class="control-label" for="input01">Type selection</label>
                                            <div class="controls">
                                                <?php echo form_dropdown('type_select', $select_array, $selected_type ? $selected_type : '0', ' id="type_select"' )?>
                                                <p class="help-block">Select item</p>
                                            </div>
                                        </div>                                      
                                    </div>                        
                                </fieldset>                                                                    
                            </form>                         
                        </div>
                        <div class="span7 well">
                        <?php if( $saved ){ ?>
                            <div id="div_order_added" class="span6 <?php echo $saved ?>">
                                    <a class="close" data-dismiss="alert">�</a>
                                    <?php echo $msg ?>
                            </div>
                        <?php } ?>                         
                            <fieldset>
                                <legend><?php echo $legend ?></legend>
                                <div name="controls">
                                <?php
                                    if( $objects ){ 
                                        echo $this->order_helper->generate_table( 
                                                $objects, 
                                                $fields,
                                                $this->table,
                                                $specials
                                        );                                        
                                    }
    
                                ?>                                     
                                </div>                        
                            </fieldset>                        
                        </div>
            </div>
