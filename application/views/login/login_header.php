<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap.css' ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/docs.css' ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/prettify.css' ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/datepicker.css' ?>"/>
<script src="<?php echo base_url() . 'application/views/assets/js/jquery.js' ?>" type="text/javascript"/></script>
<script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"/></script>
<script src="<?php echo base_url() . 'application/views/assets/js/jquery.dataTables.js' ?>" type="text/javascript"/></script>        
<script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"/></script>
<script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-tooltip.js' ?>" type="text/javascript"/></script>
<script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-popover.js' ?>" type="text/javascript"/></script>