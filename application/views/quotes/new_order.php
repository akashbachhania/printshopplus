
<?php
    if( $saved !== null){
        if( $saved === true ){         
            $saved = 'alert alert-success';
            $msg   = '<strong>Success: </strong>Your quote has been saved';                    
        } else if ( $saved === false ) {
            $saved = 'alert alert-error';
            $msg   = '<strong>Error: </strong>Your quote has not been saved (Please correct fields with red border)';            
        } else if ( $saved === 'update') {
            $saved = 'alert alert-info';
            $msg   = '<strong>Success: </strong>Your quote has been updated successfully!';             
        } else if ( $saved === 'inspector_taken'){
            $saved = 'alert alert-error';
            $msg   = '<strong>Error: </strong>I\'m sorry, '.$order->inspector->name.' is already booked for that time.  Please select another time or another inspector!';             
        } else if( $saved === 'email-sent'){
            $saved = 'alert alert-success';
            $msg   = '<strong>Success: </strong>Emails have been sent';             
        } else if( $saved === Authorize_payment::STATUS_FAILED ){
            $saved = 'alert alert-error';
            if( $order->errors ) {
                $msg   = '<strong>Error: </strong>'.implode($order->errors);
                $order->errors = array();     
            } else {
                $msg   = '<strong>Error: </strong>Transaction could not be performed at this time'; 
            }           
        } else if( $saved === Authorize_payment::STATUS_SUCCESS ){
            $saved = 'alert alert-success';
            $msg   = '<strong>Success: </strong>Credit card has been charged';
            $order->errors = array();             
        }
    } else {
        $saved = '';                         
        $msg  = '';
    }
$orderid=$this->uri->segment(3);
if($this->uri->segment(2)== 'delete')
{
	$orderid="";
}

if(is_array($errors) and count($errors) > 0) {
    $saved = 'alert alert-error';
    $msg = ($msg !='') ? $msg.'<br />' : '<strong>Error: </strong><br />';
    foreach($errors as $key => $error) {
        if(is_array($error) and count($error) > 0) {
            foreach($error as $newK => $newValue)
                $msg .= ucfirst($key).'  '.ucfirst($newK).':  '.$newValue.'<br />';
        } else {
            $msg .= ucfirst($key).' '.$error.'<br />';
        }

    }
}
    
$posturl= isset($orderid)? site_url('quotes/new_order/'.$orderid) : site_url('quotes/new_order');
?>      
<script type="text/javascript">
    <?php if($orderid > 0) {?>
    function reorder() {
        if(confirm('Are you sure you want to create a new quote with these same details?')) {
            document.location.href = '<?php echo base_url()?>quotes/duplicate_order/<?php echo $orderid;?>';
        }
        
    }
    <?php }?>
	function showupload()
	{		
		if(document.getElementById("uploadon").value==1){
			new vpb_multiple_file_uploader
			({
				vpb_form_id: "form_order",
				autoSubmit: true,
				vpb_server_url: "<?php echo base_url() . 'application/views/orders/vpb_uploader.php' ?>" // PHP file for uploading the browsed filese as wish.
			});
			document.getElementById("uploadon").value=2;
		}
	}
function checkform(formid){
	if(formid==1){		
		document.getElementById('form_order').action = "<?php echo $posturl;?>";		
		//document.getElementById('form_order').submit();
	}else if(formid==3){
		
		var order_id = $("input[name=order_id]").val();
		var dataString = $("#form_order").serialize();
		
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('quotes/save_order_pdf');?>/"+order_id,
			data: dataString,
			cache: false,
			beforeSend: function() 
			{
			},
			success: function(response)
			{
				var print_url='';
				var ret_val  = jQuery.parseJSON(response);
				if(ret_val["save_order"] == "updated"){
					print_url = "<?php echo site_url('quotes/print_order_pdf');?>/"+order_id;
					window.open(print_url);
                                        window.location.href="<?php echo site_url('quotes/show_order');?>/"+order_id;
				}
				else{
					print_url = "<?php echo site_url('quotes/print_order_pdf');?>/"+ret_val["save_order"];
					window.open(print_url);
					window.location.href="<?php echo site_url('quotes/show_order');?>/"+ret_val["save_order"];
				}				
			}
		});
	}
	else if(formid==2){
		var validate=true;
		if(document.getElementById("from").value==""){
			alert("Please enter from id");
			document.getElementById("from").focus();
			validate=false;
			return false;
		}else{
			var email = document.getElementById("from").value;
			if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
				alert("Invalid E-mail address! Please re-enter.");
				document.getElementById("from").focus();
				validate=false;
				return false;	
			}
		}
		if(document.getElementById("to").value==""){
			alert("Please enter to id");
			document.getElementById("to").focus();
			validate=false;
			return false;
		}else{
			var email = document.getElementById("to").value;
			if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
				alert("Invalid E-mail address! Please re-enter.");
				document.getElementById("to").focus();
				validate=false;
				return false;	
			}
		}
		if(document.getElementById("subject").value==""){
			alert("Please enter subject");
			document.getElementById("subject").focus();
			validate=false;
			return false;
		}
		if(document.getElementById("message").value==""){
			alert("Please enter message");
			document.getElementById("message").focus();
			validate=false;
			return false;
		}				
		if(validate){
			document.getElementById("ajax_url").value="<?php echo base_url() . 'application/views/orders/vpb_uploader.php' ?>";
			if(document.getElementById("uploadon").value==1){				
				new vpb_multiple_file_uploader
				({
					vpb_form_id: "form_order",
					autoSubmit: true,
					vpb_server_url: document.getElementById("ajax_url").value // PHP file for uploading the browsed filese as wish.
				});
			}
		}
	}
}
</script>

<section class="wrapper retracted scrollable">
    <script>
        if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
            if ($.cookie('protonSidebar') == 'retracted') {
                $('.wrapper').removeClass('retracted').addClass('extended');
            }
            if ($.cookie('protonSidebar') == 'extended') {
                $('.wrapper').removeClass('extended').addClass('retracted');
            }
        }
    </script>
           
    <nav class="user-menu">
        <a href="javascript:;" class="main-menu-access">
            <i class="icon-proton-logo"></i>
            <i class="icon-reorder"></i>
        </a>
    </nav>
    
    <ol class="breadcrumb breadcrumb-nav">
        <li><a href="."><i class="icon-home"></i></a></li>
        <li class="active">
            <a class="bread-page-title" data-toggle="dropdown" href="#"></a>
            <ul class="dropdown-menu dropdown-menu-arrow" role="menu">
            </ul>
        </li>
    </ol>
    
   <div id="top" class="panel panel-default panel-block panel-title-block">
        <div id="div_second_members"  class="span12">    
            <form id="form_order" action="javascript:void(0);" method="post" enctype="multipart/form-data">
                <div id="div_order_details" class="span12 quotes_container">
                <?php if( $saved ){ ?>
                    <div id="div_order_added" class="<?php echo $saved ?>">
                            <!--<a class="close" data-dismiss="alert">x</a>-->
                            <?php echo $msg ?>
                    </div>
                <?php } ?> 
                <?php
                    echo  $this->order_helper->get_object_form(
                            $order, 
                            array('order_date','report_due','po_text','sales_rep','terms'), 
                            Order_helper::FORM_INLINE,
                            array(
                            'order_date' => array(
                                'date' => true,                                            
                            ),
                            'report_due' => array(
                                'date' => true,                                           
                            ),
                            'po_text' => array(
                                
                            ),
                            'sales_rep' =>
                                 array('element' => Order_helper::ELEMENT_DROPDOWN,
                                       'options' => $salesrep,
                                       'selected'=> $order->sales_rep,
                                      
                                    ),                                                                               
                            'terms' => 
                                array('element'=> Order_helper::ELEMENT_DROPDOWN,
                                      'options'=> $terms,
                                      'selected'=>$order->terms,
                                        ),
                            
                            '__legend' =>  $orderid==null?'Quote details' : 'Edit Quote # '.$orderid                                        
                        ),null,array('order_date' => 'Date','report_due' => 'Due','sales_rep' => 'Rep')
                    );
                ?>
                </div>
            </form>
        </div>
    </div>       
            
    <div class="row" id ="div_client_details">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="panel panel-default panel-block" id="div_order_client">
                    <?php  echo $this->order_helper->get_object_form($order, array('company','client::name', 'client::contact' , 'client::address','client::address_2','client::state',    'client::city','client::zip','client::phone_1','client::phone_2','client::email','client::email_2','client::email_3','client::notes','client::referred_by','shipping_method','client::id',) ,null, 
                        array(
                            'company' => array( 
                                'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                'options'  => $companylist,
                                'selected' => $order->client->id),     
                            'client::notes' => array( 
                                'element'  => Order_helper::ELEMENT_TEXTAREA),  
                            'client::state' => array( 
                                'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                'options'  => $us_states,
                                'selected' => $order->client->state ),                                        
                          'shipping_method' => array( 
                                'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                'options'  => $shipping_method,
                                'selected' => $order->shipping_method ),       
                          '__legend' => 'Client details'),
                          array('client'=>'client'),
                          array('client::name'=>'name')
                        );
                    ?>
                </div>
            </div>
            <div class="col-md-6" id="div_order_inspection">
                <?php echo $this->order_helper->get_object_form($order, array('job_name','stock','colors','size','quantity','finishing','coating','others','additional_notes'), null,
                   array(
                        'additional_notes' => 
                            array('element'=>Order_helper::ELEMENT_TEXTAREA,
                                  'rows'=>6),                                                                
                        '__legend'=>'Production details',
                      'stock' => array( 
                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                            'options'  => $stocks,
                            'selected' => $order->stock ),
                      'colors' => array( 
                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                            'options'  => $colors,
                            'selected' => $order->colors ),
                      'size' => array( 
                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                            'options'  => $sizes,
                            'selected' => $order->size ),
                      // 'quantity' => array( 
                      //       'element'  => Order_helper::ELEMENT_DROPDOWN, 
                      //       'options'  => $quantities,
                      //       'selected' => $order->quantity ),
                      'finishing' => array( 
                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                            'options'  => $finishings,
                            'selected' => $order->finishing ),
                      'coating' => array( 
                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                            'options'  => $coating,
                            'selected' => $order->coating )
                      )
                    ); 
                ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" id="div_order_notes">
            <?php echo $this->order_helper->get_object_form($order, array('order_notes'), 'largeOrderNotes',
              array(
                    'order_notes' => 
                        array('element'=>Order_helper::ELEMENT_TEXTAREA,
                              'rows'=>6),                                        
                    '__legend'=>'Quote Notes')
                ); 
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12" id="div_order_notes">
            <div class="panel panel-default panel-block">
                <div class="list-group">
                    <div class="list-group-item" id="input-fields-horizontal">
                        <h4 class="section-title">Send an email</h4>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <div class="col-lg-1"></div>
                                <label for="input-horizontal" class="col-lg-2 control-label">From</label>
                                <div class="col-lg-6">
                                    <textarea class="emailcss form-control" id="from" name="from" rows="1"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-1"></div>
                                <label for="input-horizontal-counter" class="col-lg-2 control-label">To</label>
                                <div class="col-lg-6">
                                    <textarea class="emailcss form-control" id="to" name="to" rows="1"></textarea>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
     
        
</section>
