<?php
$overrides = array(
    'report_due'=> 'Due date',
    'sales_rep' => 'Sales Rep',
    'order_notes' => 'Description',
    'client_id' => 'Quote #',
    'company_id' => 'Company',
);  
?>           
         
		<?php
		$source = $this->uri->segment(4);
		if($source == 'orders') { 
                        echo "<div id='div_view_orders' class='row div_header'>";
        	} else {
			echo "<div id='div_home' class='row div_header'>";
		}
        ?>
            	
                <div id="div_orders_listing" class="span12 well quotes_container">
                    <legend>Quotes</legend>
                    <table cellspacing="0" width="100%" class="table table-bordered table-striped dataTable">
                        <thead>
                            <tr>
                                <th style="width: 50px;"><img title="Delete" src="<?php echo base_url();?>application/views/assets/img/trash.png" name="deleteall" id="deleteall"></th>
                                <th style="width: 50px;">Edit</th>
                                <th style="width: 50px;">Order #</th>
                                <th style="width: 80px;">Order date</th>
                                <th style="width: 150px;">Company</th>
                                <th style="width: 250px;">Description</th>
                                <th style="width: 80px;">Due date</th>
                                <th>Sales Rep</th>
                                <th>Status</th>
                                <th>Operator</th>
                            </tr>
                        </thead>
                    </table>
                </div>                
            </div>