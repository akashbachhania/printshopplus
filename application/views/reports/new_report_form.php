<?php
    if( $status!== null){
        if( $status=== Reports::STATUS_SAVED ){
            $class= 'alert alert-success';
            $msg   = 'Your report has been saved';                    
        } else if ( $status=== Reports::STATUS_ERROR ) {
            $class= 'alert alert-error';
            $msg   = $status_message ? $status_message : 'Your report has not been saved (Please correct fields with red border)';            
        } else if ( $status=== Reports::STATUS_UPDATED) {
            $class= 'alert alert-info';
            $msg   = 'Your report has been updated successfully!';             
        } 
    } else {
        $status= '';                         
        $msg  = '';
        $class = '';
    }  
?>

<div class="row">
    <div id="div_new_report" class="span8">
    <?php if( isset($status) && in_array($status, array(Reports::STATUS_SAVED,Reports::STATUS_ERROR,Reports::STATUS_UPDATED)) ){ ?>
        <div id="div_status" class="<?php echo $class ?>">
                <a class="close" data-dismiss="alert">�</a>
         <?php echo $msg ?>
    </div>
    <?php } ?>
    <?php    
    echo $this->order_helper->get_object_form(
                                    $report, 
                                    array(
                                         'order_id',
                                        'agent_id',
                                        'client_id',
                                        'inspector_id',
                                        'property_id',
                                        'template_id',
                                        '_address',
                                        'inspection_date',
                                        'property_status',
                                        'attendees',
                                        '_types_selected',                                         
                                        '_general_disclosures',                                         
                                        'estimated_age',                                            
                                        'comments',
                                        'id',
                                        ) ,null, 
                                    array(
                                       'order_id' => array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $orders,
                                            'display'  => true, 
                                            'selected' => (int)$report->order_id ),                                    
                                       'agent_id' => array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $agents,
                                            'display'  => true, 
                                            'selected' => (int)$report->agent_id ),
                                      'client_id' => array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $clients,
                                            'display'  => true, 
                                            'selected' => (int)$report->client_id ),
                                      'inspector_id' => array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $inspectors,
                                            'display'  => true, 
                                            'selected' => (int)$report->inspector_id ),
                                      'property_id' => array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $properties,
                                            'display'  => true, 
                                            'selected' => (int)$report->property_id ),
                                      'template_id' => array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $templates,
                                            'display'  => true, 
                                            'selected' => (int)$report->template_id ),                                                                                        
                                      'property_status'=> array( 
                                            'element'  => Order_helper::ELEMENT_DROPDOWN, 
                                            'options'  => $statuses,
                                            'selected' => $report->property_status),  
                                      '_types_selected'=> array( 
                                            'element'     => Order_helper::ELEMENT_CHECKBOX, 
                                            'selected'    => $report->type_ids,
                                            'label'       => '<name>',
                                            'group_label' => 'Utility Status',
                                            'description' => '<description>',
                                            'id'          => 'type_id_utility_<id>'),
                                      '_general_disclosures'=> array( 
                                            'element'     => Order_helper::ELEMENT_CHECKBOX, 
                                            'selected'    => $report->type_ids,
                                            'label'       => '<name>',
                                            'group_label' => 'General Disclosures',
                                            'description' => '<description>',
                                            'id'          => 'type_id_general_<id>'),                                                                                                                                                                                                                                                                                                                     
                                      'inspection_date' => array(
                                            'date' => true ),
                                      'comments'  => array(
                                            'element'=>Order_helper::ELEMENT_TEXTAREA,
                                            'rows'=>6),
                                      'estimated_age' =>  array(
                                             'element' => Order_helper::ELEMENT_DROPDOWN,
                                             'options' => $estimated_ages,
                                             'selected'=> $report->estimated_age ? $report->estimated_age: $report->_order->estimated_age,
                                            ),
                                      '__legend' => $report->id ? 'Report for: ' . $report->_address: 'New report',
                                      '__form'   => array(
                                            'action' => site_url('reports/new_report'), 
                                            'submit_value' => Reports::ACTION_SAVE,
                                            'submit_name'  => 'report_submit',
                                            'additional_buttons' => array(
                                                    Reports::ACTION_NEXT => array(
                                                        'name' => 'report_submit',
                                                        'id'   => 'btn_next',
                                                        'type' => Order_helper::ELEMENT_SUBMIT,
                                                        'class'=> ''
                                                    ),
                                                    Reports::ACTION_BACK  => array(
                                                        'name' => 'report_submit',
                                                        'id'   => 'btn_back',
                                                        'type' => Order_helper::ELEMENT_SUBMIT,
                                                        'class'=> '' 
                                                    ),
                                                    Reports::ACTION_PDF  => array(
                                                        'name' => 'report_submit',
                                                        'id'   => 'btn_pdf',
                                                        'type' => Order_helper::ELEMENT_SUBMIT,
                                                        'class'=> '' 
                                                    ),
                                                    Reports::ACTION_HTML => array(
                                                        'name' => 'report_submit',
                                                        'id'   => 'btn_html',
                                                        'type' => Order_helper::ELEMENT_SUBMIT,
                                                        'class'=> '' 
                                                    ),
                                                    Reports::ACTION_CLOSE => array(
                                                        'name' => 'report_submit',
                                                        'id'   => 'btn_html',
                                                        'type' => Order_helper::ELEMENT_SUBMIT,
                                                        'class'=> '' 
                                                    )                                                                                                        
                                              ),
                                            'hidden'=>array(
                                                'address' => $report->address,
                                                'city' => $report->city,
                                                'state' => $report->state,
                                                'zip' => $report->zip,
                                                'order_id' => $report->order_id
                                             )
                                            )                                      
                                      ),
                                      array(
                                        //Form field namespaces
                                        '_client'    => 'client',
                                        '_agent'     => 'agent',
                                        '_inspector' => 'inspector',
                                        '_order'    => 'order',
                                        '_property' => 'property',
                                        '_template' => 'template'
                                      ),
                                      array(
                                        //Form fields label overrides
                                        'order_id' => 'Report for',
                                        'client_id' => 'Buyer',
                                        'agent_id' => 'Agent',
                                        'inspector_id' => 'Inspector',
                                        'property_id' => 'Property type',
                                        'template_id' => 'Report template',
                                        '_order::estimated_age' => 'Year built',
                                        '_address' => 'Address',
                                        '_all_types' => 'Utility Status',
                                        )
                                    );    
    
    ?>
    </div>
    <div id="div_new_image" style="display:none;">
    </div>
</div>
