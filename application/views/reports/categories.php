<?php
if( $status!== null){
    if( $status=== Reports::STATUS_SAVED ){
        $class= 'alert alert-success';
        $msg   = '<strong>Success: </strong>Your report has been saved';                    
    } else if ( $status=== Reports::STATUS_ERROR) {
        $class= 'alert alert-error';
        $msg   = '<strong>Error: </strong>Your report has not been saved (Please correct fields with red border)';            
    } else if ( $status=== Reports::STATUS_UPDATED) {
        $class= 'alert alert-info';
        $msg   = '<strong>Success: </strong>Your report has been updated successfully!';             
    } 
} else {
    $status= '';                         
    $msg  = '';
    $class = ''; 
}
/**
* put your comment there...
* 
* @var ReportAccordion
*/
$accordion;
?>
<div class="row">
    <?php if( isset($status) && in_array($status, array(Reports::STATUS_SAVED,Reports::STATUS_ERROR,Reports::STATUS_UPDATED)) ){ ?>
         <div id="div_status" class="span12 <?php echo $class ?>">
                <a class="close" data-dismiss="alert">�</a>
         <?php echo $msg ?>
         </div>
    <?php } ?>    
</div>
<div class="row">
    <div class="categories_menu span3">
        <div class="span3 bs-docs-sidebar">
            <ul class="nav nav-tabs nav-stacked">    
                <?php if($report_categories = $report->get_categories() ) { ?>
                      <?php foreach( $report_categories->categories as $categories ){ ?>
                          <?php foreach( $categories as $index=>$category ){ ?>
                                <li class="category"><a href="#" class="category_button <?php echo ($category_id == $category->id) && $category->_index == $index ? 'selected' : '' ?>" name="<?php echo $category->name ?>" id="<?php echo $category->id.'_'.$category->_index?>"><?php echo (strlen($category->name)>14 ? substr($category->name,0,14).'...': $category->name) .( count($categories)>1 || $category->_index>1 ? ' #'.($category->_index+1).'' : '' )?></a><i class="category_add icon-plus pull-right" stype="display:none;"></i><i class="category_remove icon-minus pull-right"  stype="display:none;"></i></li>        
                          <?php } ?>
                      <?php } ?>
                <?php } ?>
                 <li class="category"><a class="toggle_all btn">Toggle all</a></li>
                 <li class="category"><a class="toggle_all btn" target="_blank" href="<?php echo site_url('reports/report_html')?>">Preview HTML</a></li>
                 <li class="category"><a class="toggle_all btn" target="_blank" href="<?php echo site_url('reports/report_pdf')?>">Preview PDF</a></li>
                 <li class="category"><a class="toggle_all btn" href="<?php echo site_url('reports/new_report?order_id='.$report->order_id)?>">Back</a></li>
            </ul>
        </div>
    </div>
    <div id="div_report" class="offset1 span7">
        <form id="form_categories" action="<?php echo site_url('reports/categories')?>" method="post">            
            <?php if( $category_id && $category_index !== null && ($categories = $report->get_categories()) ){ ?>
                <input type="hidden" name="submit_category" value="<?php echo $category_id.'_'.$category_index ?>">
                <input type="hidden" name="report_id" value="<?php echo $report->id ?>" id="report_id">
                <div id="accordition">
                    <?php echo $accordion->get_category_container($categories, $category_id, $category_index) ?>                
                </div>                
            <?php }?>
        </form>
    </div>
</div>
<div class="modal hide fade image_edit_modal" id="image_edit_modal" style="max-height:auto;">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">�</a>
        <h3 id="h_modal_header">Edit Image</h3>
    </div>
    <div id="div_aviary" class="modal-body">
        <img id="img_edit" src="">
    </div>
    <div class="modal-footer">
        <a class="btn add_image"  id="btn_charge_modal">Add Image</a>
        <a class="btn edit_image" id="btn_charge_modal">Edit Image</a>
        <a class="btn delete_image" id="btn_charge_modal">Delete</a>
        <a data-dismiss="modal" href="#" class="btn">Close</a>
    </div>
</div>

<?php 







