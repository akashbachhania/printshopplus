<?php                       
$overrides = array(
    'status_string::name'=> 'Status',
    'term::name'         => 'Terms',
    'agent::name'        => 'Agent',
);

if( $status == Order::ORDER_STATUS_REPORT_STARTED ){ $title = 'Started reports'; } 
else { $title = 'All reports'; }
?>           
         

<div id="div_view_reports" class="row div_header">
    <div id="div_orders_table" class="span12 well">
        <legend><?php echo $title ?></legend>
        <?php echo $this->order_helper->get_order_table( $orders, array(
                    'id',
                    'status_string::name',
                    'agent::name',
                    //'order_date',
                    'inspection_date',
                    'address',
                    'city',
                    'total',
                    'paid',
                    'amount_due'
              ), 
              $this->html_table,
              $overrides);
        ?>
    </div>                
</div>
