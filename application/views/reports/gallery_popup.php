<div class="modal hide fade image_modal" id="image_modal_<?php echo $subcategory_id ?>" style="max-height:auto;">
    <div class="modal-header">
        <a class="close" data-dismiss="modal">�</a>
        <h3 id="h_modal_header">Images</h3>
    </div>
    <div class="modal-body">                           
        <div id="div_galleria_<?php echo $subcategory_id ?>">
            <?php if($images){ ?>
                <?php foreach($images as $image){?>
                    <img src="<?php echo $image?>">
                <?php } ?>
            <?php }?>
        </div>
        <div id="div_add_image_<?php echo $subcategory_id ?>" style="display:none;">
            <a id="images_button_<?php echo $subcategory_id ?>">Upload files</a>
        </div>
        <input id="image_subcategory_<?php echo $subcategory_id ?>" type="hidden" value="">                    
    </div>
    <div class="modal-footer">
        <a class="btn add_image">Add Image</a>
        <a class="btn edit_image">Edit Image</a>
        <a class="btn delete_image">Delete</a>
        <a data-dismiss="modal" href="#" class="btn">Close</a>
    </div>
</div>

