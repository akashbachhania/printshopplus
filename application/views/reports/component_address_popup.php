<div class="modal hide fade address_modal" id="address_modal" style="max-height:auto;">
    <div class="modal-header">
        <h3 id="h_modal_header">Report address</h3>
    </div>
    <div class="modal-body">
        <div class="form-horizontal">
            <div name="controls">                                 
                <div class="control-group fill_in">
                    <label class="control-label" for="subtotal">Address</label>                          
                    <div class="controls">                           
                         <input id="modal_address" class="input-medium" type="text" value="<?php $report->address ?>" name="address">
                    </div>
                </div> 
                <div class="control-group fill_in">
                    <label class="control-label" for="subtotal">City</label>                          
                    <div class="controls">                           
                         <input id="modal_city" class="input-medium" type="text" value="<?php $report->city ?>" name="city">
                    </div>
                </div>
                <div class="control-group fill_in">
                    <label class="control-label" for="subtotal">State</label>                          
                    <div class="controls">                           
                         <?php echo form_dropdown('modal_state', $states, $report->state,' id="state" class="input-medium" ')?>
                    </div>
                </div>    
                <div class="control-group fill_in">
                    <label class="control-label" for="subtotal">Zip</label>                          
                    <div class="controls">                           
                         <input id="modal_zip" class="input-medium" type="text" value="<?php $report->zip ?>" name="zip">
                    </div>
                </div>                                                                                                                                                                                                                                                                                                                             
            </div>        
        </div>        
    </div>
    <div class="modal-footer">
        <a id="btn_modal_save_address" class="btn" >Save address</a>
        <a data-dismiss="modal" href="#" class="btn">Close</a>
    </div>
</div>
