<div class="row row_html">
    <div class="categories_menu span2">
        <div class="span2 bs-docs-sidebar">
            <form id="form_categories" action="<?php echo site_url('reports/report_html')?>">
                <ul class="nav nav-tabs nav-stacked">
                    <li class="category"><a href="#" class="category_button_html <?php echo $category_id == Reports::HTML_PREFACE ? 'selected' : '' ?>" name="" id="<?php echo Reports::HTML_PREFACE?>">Preface</a></li>
                    <li class="category"><a href="#" class="category_button_html <?php echo $category_id == Reports::HTML_DETAILS ? 'selected' : '' ?>" name="" id="<?php echo Reports::HTML_DETAILS?>">Report Details</a></li>
                    <li class="category"><a href="#" class="category_button_html <?php echo $category_id == Reports::HTML_DISCLOSURES ? 'selected' : '' ?>" name="" id="<?php echo Reports::HTML_DISCLOSURES?>">Disclosures</a></li>
                    <?php foreach( $report->get_categories()->categories as $categories ){ ?>
                          <?php foreach( $categories as $index=>$category ){ ?>
                                <?php if( $category->validate() ) {?>
                                    <li class="category"><a href="#" class="category_button_html <?php echo $category_id == $category->id && $category_index == $category->_index? 'selected' : '' ?>" name="<?php echo $category->name ?>" id="<?php echo $category->id.'_'.$category->_index?>"><?php echo (strlen($category->name)>14 ? substr($category->name,0,14).'...': $category->name) .( count($categories)>1 || $category->_index>1 ? ' #'.($category->_index+1).'' : '' )?></a></li>        
                                <?php } ?>
                          <?php } ?>
                    <?php } ?>
                </ul>
            </form>
        </div>
    </div>
    <div id="div_report_html" class="offset1 span9">
        <?php if( $category_id && $category_index !== null ){ ?>
            <div><span class="span_category"><?php echo $report->get_categories()->categories[$category_id][$category_index]->name?></span></div>
            <div class="subcategory_container">
                <!--Iterate through all subcategories of selected category-> 
                <?php foreach( $report->get_categories()->categories[$category_id][$category_index]->_subcategories as $index=>$subcategory ){?>
                    <!--Is this a valid category-->
                    <?php if( $subcategory->validate() ){?>
                    
                        <div class="div_subcategory_title">
                            <p class="p_subcategory"><?php echo clean($subcategory->name)?></p>
                            <p class="p_rating"><?php echo $subcategory->_rating ?></p>
                        </div>
                        
                        <div class="div_subcategory_values">
                            <div>
                                <!--Subcategory values-->              
                                <?php echo get_values_section($subcategory) ?>
                            </div>
                            <div>
                                <?php echo $subcategory->comments ? '<span class="span_notes">Notes:</span><p class="p_notes">'. $subcategory->comments.'</p>' : '' ?>
                            </div>
                        </div>
                        
                        <!--Subcategory pictures--> 
                        <?php if($report->_pictures[$subcategory->subcategory_id]) {?>
                            <?php echo get_gallery_div($report->_pictures[$subcategory->subcategory_id], $subcategory->id)?>                       
                        <?php }?>
                                                                           
                     <?php }?>
                <?php }?>
                <?php } else if( $category_id == Reports::HTML_PREFACE) {?>
                    <?php echo get_preface($report) ?>
                <?php } else if( $category_id == Reports::HTML_DISCLOSURES) { ?>
                    <?php echo get_additional_information($report) ?>
                <?php } else if( $category_id == Reports::HTML_DETAILS){?>
                    <?php echo get_report_details($report) ?>
                <?php } ?>                
            </div>

    </div>
</div>
                    
                    
                    


<?php
function clean( $string ){
    return str_replace('Good, Fair, Poor, None, N/A', '', $string);
}
function get_gallery_div( $pictures, $subcategory_id ){
    $imgs = '';
    foreach( $pictures as $picture ){
        $imgs .='<img height="800px" class="img-polaroid img-rounded" src="'.$picture->path.'">';
    }
    
    return   
    '<div id="div_carousel">
        <div id="div_report_galleria_'.$subcategory_id.'">
            '.$imgs.'
        </div>
    </div';     
}
function get_additional_information(Report $report){ 
    foreach( $report->_general_disclosures as $disclosure ){
        if( is_numeric($disclosure->id)){
            $list[] = '<dt>'.$disclosure->name.'</dt>';   
            $list[] = '<dd>'.$disclosure->description.'</dd>';   
        }
    }
    foreach( $report->_types_selected as $disclosure ){
        if( is_numeric($disclosure->id)){
            $list[] = '<dt>'.$disclosure->name.'</dt>';   
            $list[] = '<dd>'.$disclosure->description.'</dd>';     
        }
    } 
    $html = '';
    if($list){
        $html =   
            '<span class="span_category span_predefined">General Disclosures</span></span>
             <div class="subcategory_container padding">
                <div class="div_disclosure">
                     <dl>'.implode('',$list).'</dl>
                </div>
            </div>';            
    } 
    
    if($report->comments){
        $html .=
           '<span class="span_category span_predefined">Additional Disclosures</span>
            <div class="subcategory_container padding">
                <div class="div_disclosures">'.$report->comments.'</div>
            </div>';            
    } 
    return $html;   
}
function get_report_details(Report $report){
        
    $fields = array(
        'Report #' => $report->_report_number,
        'Type of Property' => $report->_property_type,
        'Address' => $report->_address,
        'Inspector name' => $report->_order->inspector->name,
        'Date of inspection' => $report->_order->inspection_date,
        'Buyer' => $report->_order->client->name,
        'Agent' => $report->_order->agent->name,
        'Year built' => $report->estimated_age,
        'Attendees' => $report->attendees,
        'Property status' =>$report->property_status
    );
    foreach( $fields as $field=>$value){
        if( $value !== null ){
            $rows .= '<tr><td class="td_field"><b>'.$field.':</b></td><td class="td_value">'.$value.'</td></tr>';                            
        }
    }

    $html = 
   '<span class="span_category span_predefined">Your report details</span>
    <div class="subcategory_container padding">
      <table class="table_details">
        <tbody>'
            .$rows.
        '</tbody>
      </table>
    </div>';
             
    return $html;    
}

function get_preface(Report $report){
    
    
    $pictures = '';
    if( $report->_pictures[0]){
        $pictures =
        '<div class="subcategory_container padding">
            '.get_gallery_div($report->_pictures[0], 0).'
        </div>';
    }
    return 
    '<p class="preface">'.$report->_address.'</p>
    '.$pictures;           
}
 

function get_picture_rows( $pictures ){ 
    $row = array();
    $rows = '';
    if( is_array($pictures)){
        foreach( $pictures as $picture ){
                if( !isset($active) ){
                    $active = 'active';
                } else {
                    $active = '';
                }
                $rows .= '<div class="item '.$active.'"><img height="800px" class="img-polaroid img-rounded" src="'.$picture->path.'"></div>';
                    
        } 
        if($row){
            $rows .= '<tr>'.implode('',$row).'</tr>'; 
        }            
    }
    return $rows;
}       
function get_values_section( Subcategory $subcategory ){
    $html = array();
    foreach( $subcategory->_value_ids as $id){
        $concern_list = array();
        if( isset($subcategory->_values[$id]->_concerns)){
            foreach($subcategory->_values[$id]->_concerns as $concern){
                if( in_array($concern->id, $subcategory->_values[$id]->_concern_ids)){
                    $concern_list[] = '<li>'.$concern->name.'</li>';
                }
            }
        }
        $html[] = '<li>'.$subcategory->_values[$id]->name. ($concern_list? '<ul>'.implode('',$concern_list).'</ul>' : '').'</li>';
    }
    return '<ul>'.implode('', $html).'</ul>';
}
