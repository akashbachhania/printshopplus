<?php
$overrides = array(
    'status_string::name'=> 'Status',
    'term::name'         => 'Terms',
    'agent::name'        => 'Agent',
    'inspector::name'    => 'Inspector',
    'inspection::name'   => 'Inspection',
    'inspection_time'    => 'Time',
    'inspection_date'    => 'Date',
    'phone_1'            => 'Phone',
    'agent::name'        => 'Client',
);
$message = new Message();

foreach( $orders as &$order){
    $order->set_inspection_time( $times[$order->inspection_time], true);
}
?>
<?php if($user->person_type != Person::TYPE_INSPECTOR ){?>
     <div id="div_home_buttons" class="row">
<?php /*
        <div class="span2">

            <a id="a_home_new_orders"  class="a_center" href="<?php echo site_url('orders/new_order/')?>"></a>

        <ul class="letsroll" id="iconbar">
       <li class="letsroll" style="font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#999999; font-size:14px;">LEARN MORE<img src="http://www.homeinspectorplus.com/live/application/views/assets/img/uvx.gif" alt="" />
        <h5>New orders allows you to easily create a new order. You'll be able to create an order that you can send to your client and inspector all at once.<br />
          <br />
            <a href="#">View instructional video by clicking here.</a><br />
        </h5></li></ul>

        </div>

        <div class="span2 offset2">

            <a id="a_home_schedule" class="a_center" href="<?php echo site_url('schedule')?>"></a>

        <ul class="letsroll" id="iconbar">
          <li class="letsroll" style="font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#999999; font-size:14px;">LEARN MORE <img src="http://www.homeinspectorplus.com/live/application/views/assets/img/uvx.gif" alt="" />
            <h5>View schedule allows you to easily manage all your inspections. If you have more than one inspector or if it's just yourself you'll easily see all your upcoming inspections.<br />
                <a href="#"><br />
                View instructional video by clicking here.</a><br />
          </h5></li></ul>

        </div>

        <div class="span2 offset2" >

            <a  id="a_home_report"  class="a_center" href=""></a>

        <ul class="letsroll" id="iconbar">
        <li class="letsroll" style="font-family:Arial, Helvetica, sans-serif; font-weight:bold; color:#999999; font-size:14px;">LEARN MORE<img src="http://www.homeinspectorplus.com/live/application/views/assets/img/uvx.gif" alt="" />
        <h5>Create report  let's you make a detailed inspection report. Our easy to use report maker will help you make a detailed inspection report.<br />
            <a href="#"><br />
            View instructional video by clicking here.</a><br />
        </h5></li></ul>

      </div>        
*/ ?>
    </div>
<?php }?>
<!--
<div id="div_home" class="row">

    <div class="span8" id="div_orders_table">
        <div class="well"> 
            <legend id="lbl_orders">Orders</legend>
            <div name="controls">
                <?php  
                    echo $this->order_helper->get_order_table( $orders, $order_fields, $this->html_table, $overrides, ($user->person_type == Person::TYPE_INSPECTOR ? false: true) );
                ?>     
            </div>
        </div>
    </div>
    <div class="span4" id="div_home_messages">
        <div class="well">
            <legend id="lbl_messages">Messages</legend>
             <div name="controls">
                <div id="div_home_new_msg">
                    <a class="btn" id="btn_home_new_msg" href="">New message</a> 
                </div>
                <div class="hidden" id="home_message_reply">
                    <h4>New message</h4>
                    <input type="hidden" name="from_user_id" value="<?php echo $message->to_user_id ?>"/>
                    <input type="hidden" name="datetime" value="<?php echo gmdate('Y-m-d h:i:s')?>"/>
                    <div>
                        <?php
                            $this->table->clear();
                            $this->table->add_row( array('<strong>To:</strong>', get_group_dropdown( 'to_user_id', $recipients, $message->to_user_id  ) ));
                            $this->table->add_row( array('<strong>Type:</strong>', form_dropdown( 'type', array( Message::TYPE_MSG => 'Message', Message::TYPE_TODO=>'Task')  )  ));
                            $this->table->add_row( array('<strong>Message:</strong>', '<textarea name="message" rows="5" value=""></textarea>' ));
                            echo $this->table->generate();             
                        ?>
                        <a class="btn" name="btn_send" href="">Send</a>
                        <a class="btn" name="btn_close" href="">Close</a>                 
                    </div>
                </div>            
                <div id="div_home_all_msg">
                <?php
                if( $messages ){ 
                    echo $this->order_helper->generate_table( $messages,array(
                        '_user_from::name',
                        'subject',
                        'datetime'
                        ),
                        $this->table,
                        array(
                            /*'type' => array(
                                'values' => array(
                                    Message::TYPE_MSG  => '<i mesage_id="5555" class="icon-envelope"></i>',
                                    Message::TYPE_TODO => '<i class="icon-pencil"></i>',
                                    'default' => '<value>'
                                )
                            ),*/
                            '_user_from::name'=> array(
                                'name' => 'From'
                            ),
                            'subject' => array(
                                'href' => '#',
                                'id' => '<id>',
                                'name' => 'Message',
                            )
                        )
                    );          
                }
                ?>
                </div>            
            </div>        
        </div>
    </div>    
</div>-->
<?php 
function get_group_dropdown( $name='', $companies, $id='', $selected="" ){
    $html = '<select name="'.$name.'" id="'.$id.'" multiple="multiple" >';
    foreach( $companies as $company=>$persons){
        $html .= '<optgroup label="'.$company.'">';
        foreach( $persons as $id=>$name){
            $html .= '<option value="'.$id.'">'.$name.'</option>' ;    
        }
        $html .= '</optgroup'; 
    }
    $html .= '</select>';
    return $html;
}
?>