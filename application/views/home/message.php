<?php
  
?>
<div class="row-fluid">
    <h4><?php echo $message->_user_from->name . ' on: '. $message->datetime ?></h4>
    <div class="span6">
        <?php
            $this->table->clear();
            $this->table->add_row( array('<strong>From:</strong>', $message->_user_from->name ));
            $this->table->add_row( array('<strong>Subject:</strong>', $message->subject ));
            $this->table->add_row( array('<strong>Date:</strong>', $message->datetime ));
            $this->table->add_row( array('<strong>Message:</strong>', '<textarea name="message" rows="5" value="'.htmlspecialchars($message->message).'"></textarea>' ));
            echo $this->table->generate();
        ?>
        <a class="btn" id="btn_home_reply" href="">Reply</a>
        <div class="hidden" id="home_message_reply">
            <input type="hidden" name="to_user_id" value="<?php echo $message->from_user_id ?>"/>
            <input type="hidden" name="from_user_id" value="<?php echo $message->to_user_id ?>"/>
            <input type="hidden" name="datetime" value=""/>
            <?php
            $this->table->clear();
            $this->table->add_row( array('<strong>Subject:</strong>', 'RE:' .$message->subject ));
            $this->table->add_row( array('<strong>Message:</strong>', '<textarea name="message" rows="5" value=""></textarea>' ));
            echo $this->table->generate();             
            ?>
            <a class="btn" href="">Send</a>
            <a class="btn" href="">Close</a>
        </div>
    </div>
</div>

