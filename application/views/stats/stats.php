<?php
    $fluid_layout = true;
    if( isset( $fluid_layout ) && $fluid_layout == true ){
        $class = 'container-fluid';
    } else {
        $class = 'container';
    }
    $class = 'container';
    
    
if( $orders )  {
    
    $total_orders = count( $orders );
    $total_paid   = 0;
    $total_due    = 0;
    $total_grand  = 0;
    $total_tax    = 0;
    $shipping_amount = 0;
    
    $orders_inspector = array();
    $orders_clients = array();
    $orders_items = array();
    foreach( $orders as $order ){
        $total_paid  += $order->paid;
        $total_due   += $order->amount_due;
        $total_grand += $order->total;
        if($order->tax > 0) {
            $total_tax   += ($order->total / 100) * $order->tax;
        }
        $shipping_amount += $order->shipping_amount;
        
        @$orders_inspector[ $order->inspector->name ]++;
        @$orders_clients[ $order->client->name ]++;
        
        if(is_array($order->items)) {
            foreach ($order->items as $item)
                @$orders_items[ $item->name ]++;
        }
       
    }
    $total_tax = ($total_tax > 0) ? round($total_tax,2) : $total_tax;
    $total_grand = ($total_grand > 0) ? round($total_grand,2) : $total_grand;
    $total_paid = ($total_paid > 0) ? round($total_paid,2) : $total_paid;
    $total_due = ($total_due > 0) ? round($total_due,2) : $total_due;
    $shipping_amount = ($shipping_amount > 0) ? round($shipping_amount,2) : $shipping_amount;
}    
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Print Shop Plus</title>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/bootstrap.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/docs.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/prettify.css' ?>"/>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() . 'application/views/assets/style/datepicker.css' ?>"/>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-datepicker.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/jquery.dataTables.js' ?>" type="text/javascript"/></script>        
        <script src="<?php echo base_url() . 'application/views/assets/js/custom.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-tooltip.js' ?>" type="text/javascript"/></script>
        <script src="<?php echo base_url() . 'application/views/assets/js/bootstrap-popover.js' ?>" type="text/javascript"/></script>
        <?php if( $orders )  { ?>

        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">

          // Load the Visualization API and the piechart package.
          google.load('visualization', '1.0', {'packages':['corechart']});

          // Set a callback to run when the Google Visualization API is loaded.
          google.setOnLoadCallback(drawChart);

          // Callback that creates and populates a data table,
          // instantiates the pie chart, passes in the data and
          // draws it.
          function drawChart() {
            var data;
            var options;
            var chart;
            
            // Create the data table.
            data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows([
                    ['Paid', <?php echo $total_paid ?>],
                    ['Unpaid', <?php echo $total_due ?>],
            ]);
            options = {'title':'Payment of orders',
                           'width':400,
                           'height':300};

            // Instantiate and draw our chart, passing in some options.
            chart = new google.visualization.PieChart(document.getElementById('chart_payment'));
            chart.draw(data, options);                        
            <?php 
                $summaries = array(
                    'orders_clients',                    
                    'orders_inspector',
                    'orders_items',
                );
                
                $divs = array(
                    'orders_clients'      => 'chart_clients',
                    'orders_inspector' => 'chart_inspector',
                    'orders_items'     => 'chart_items',
                );
                $titles = array(
                    'orders_clients'      => 'Orders by client',
                    'orders_inspector' => 'Orders per Sales Rep',
                    'orders_items'     => 'Orders by item',
                );                
                foreach( $summaries as $summary ){
                    if( !$summary ) continue;
                ?>
                // Create the data table.
                data = new google.visualization.DataTable();
                data.addColumn('string', 'Topping');
                data.addColumn('number', 'Slices');
                data.addRows([
                <?php
                
                    foreach( $$summary as $title=>$value ){
                        echo "['$title', $value],";
                    }
                ?>  
                  
                ]);

                // Set chart options
                options = {'title':'<?php echo $titles[ $summary ] ?>',
                               'width':400,
                               'height':300};

                // Instantiate and draw our chart, passing in some options.
                chart = new google.visualization.PieChart(document.getElementById('<?php echo $divs[ $summary ]?>'));
                chart.draw(data, options);
                <?php } ?>
          }
        </script>
        <?php }?>
    </head>
     <body>                  
         <?php if( !isset( $dont_display_header )) {?>
            <div id="div_header_top" class="navbar">
                <div class="navbar-inner">
                    <div id="div_header_container" class="container">                        
                        <div class="nav-collapse pull-left">
                            <ul class="nav">
                                <li><a class="" href="#"><?php echo $user->name ?></a></li>
                                <li class="divider-vertical"></li>
                                <li><a class="" href="<?php echo site_url('home/settings')?>" target="__blank">My Settings</a></li>
                                <li class="divider-vertical"></li>
                                <li><a class="" href="<?php echo site_url('login')?>">Sign Out</a></li>
                                <li class="divider-vertical"></li>
                                <li><a class="" href="<?php echo site_url('home')?>">Support</a></li>                        
                            </ul>
                            <?php if( in_array( $user->person_type, array(Person::TYPE_ADMIN))){ ?>
                                <form class="navbar-form pull-left">
                                    <label id="label_admin_company" class="checkbox"></label> 
                                    <?php echo form_dropdown('admin_company_id', $companies, $user->company_id ? $user->company_id : '0', ' id="select_admin_view_companies" class="span2"'  ); ?>
                                </form>
                            <?php }?>                                  
                        </div>
                        <div id="div_logo"></div>
                    </div>
                 </div>
            </div>
            <div id="div_header_navbar" class="navbar">
                <div class="navbar-inner">
                    <div id="div_header_container_lower">
                        <div id="inner" class="nav-collapse pull-left"> 
                            <ul id="ul_header_submenu" class="nav">
                                <li><a id="a_home"  href="<?php echo site_url('orders/view_orders/all/home')?>">Home</a></li>
                                
                                <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN, Person::TYPE_USER )) ) {?>
                                    <li><a id="a_orders"  href="<?php echo site_url('orders/new_order')?>"> Orders</a></li>
                                    <li><a id="a_clients" href="<?php echo site_url('clients')?>"> Clients</a></li>
                                    <li><a id="a_stats"   href="<?php echo site_url('stats')?>"> Stats</a></li>
                                    <li><a id="a_inspectors"  href="<?php echo site_url('inspectors')?>"> Sales Rep</a></li>                                                         
                                <?php } ?>
                                
                                <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN, Person::TYPE_USER, Person::TYPE_INSPECTOR )) ) {?>
                                    <li><a id="a_schedule"  href="<?php echo site_url('schedule')?>"> Schedule</a></li>
                                <?php } ?>
                                <?php if( in_array($user->person_type, array(Person::TYPE_ADMIN)) ) {?>
                                    <li><a id="a_admin" href="<?php echo site_url('admin')?>">Admin</a></li>
                                                         
                                <?php } ?>                
                            </ul>
                        </div>
                    </div>
                </div>    
            </div>
        <?php } 
        
        if( isset($new_order )){
        ?>
        <div id="order_submenu" class="subnav">
            <ul class="nav nav-tabs"> 
                    <li><a class='abtn-primary' href="<?php echo site_url('orders/new_order')?>"><i class=""></i> New Orders</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url('orders/view_orders/' . Orders::VIEW_ALL)?>"><i class=""></i> View All</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url('orders/view_orders/' . Order::ORDER_STATUS_PENDING)?>"><i class=""></i> View Pending</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url('orders/view_orders/' . Order::ORDER_STATUS_DISPATCHED)?>"><i class=""></i> View Dispatched</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url('orders/view_orders/' . Order::ORDER_STATUS_PENDING_REPORT)?>"><i class=""></i> View Pending Report</a></li>                    
                    <li><a class='abtn-primary' href="<?php echo site_url('orders/view_orders/' . Order::ORDER_STATUS_UNPAID)?>"><i class=""></i> View Unpaid</a></li>
                    <li><a class='abtn-primary' href="<?php echo site_url('orders/view_orders/' . Order::ORDER_STATUS_CLOSED)?>"><i class=""></i> View Completed</a></li>    
            </ul>
        </div>
        <?php } ?>    
       <div style="min-height: 100%;">
        <div class="<?php echo $class ?> clearfix">
            <section id="gridSystem">
                <div class="row">
                    <div id="div_stats_filter" class="span5">
                        <form class="form-horizontal well" action="<?php site_url("stats/show_stats")?>" method="post">
                            <fieldset>
                                <legend>Stats filter</legend>
                                    <div class="control-group">
                                        <label class="control-label" for="start_date">Date from:</label>
                                        <div class="controls">
                                            <input  data-datepicker="datepicker" type="text" class="span2" name="start_date" id="start_date" value="<?php echo isset($start_date) ? $start_date : '' ?>">
                                        </div>
                                    </div>
                                    <div class="control-group"> 
                                        <label class="control-label" for="end_date">Date to:</label>
                                        <div class="controls">
                                            <input  data-datepicker="datepicker" type="text" class="span2" name="end_date" id="end_date" value="<?php echo isset($end_date) ? $end_date : '' ?>">
                                        </div>
                                    </div>
                                   <div class="control-group">                         
                                        <label class="control-label" for="person_type">Filter</label>
                                        <div class="controls">
                                            <select  class="span2" id="person_type" name="person_type">
                                                <option value="ALL" <?php echo (isset($person_type) and $person_type == 'ALL') ? 'selected="selected"' : '' ?> >All Orders</option>                                                
                                                <option value="<?php echo Person::TYPE_AGENT; ?>" <?php echo (isset($person_type) and $person_type == Person::TYPE_AGENT) ? 'selected="selected"' : '' ?>>By Client</option>
                                                <option value="<?php echo Person::TYPE_INSPECTOR; ?>" <?php echo (isset($person_type) and $person_type == Person::TYPE_INSPECTOR) ? 'selected="selected"' : '' ?>>By Sales Rep</option>
                                            </select>
                                        </div>
                                   </div>
                                   <div id="div_stat_persons" class="control-group">                         
                                        <label class="control-label" for="person_id">Name</label>
                                        <div class="controls">
                                            <?php echo form_dropdown('person_id', $persons, $person_id, ' class="span2" id="person_id" ')?>
                                        </div>
                                   </div>  
                                
                                
                                   <div class="control-group">
                                        <label class="control-label" for="submit"></label>
                                        <div class="controls">
                                            <input id="submit" name="submit" type="submit" class="span2 btn" value="Submit">
                                        </div>                                                                                                  
                                   </div>
                            </fieldset>
                        </form>    
                    </div>     
    <div id="div_stats_result" class="span7">
        
        <?php
             
            if( $orders ){
                ?>
                <div class="span4">
                    <h4>Orders summary:</h4>                
                    <?php
                        $this->table->clear();
                        $this->table->add_row("<strong>Total #</strong>", $total_orders);
                        $this->table->add_row("<strong>Total amount (\$) </strong>", $total_grand);
                        $this->table->add_row("<strong>Total paid (\$)</strong>", $total_paid);
                        $this->table->add_row("<strong>Total due (\$)</strong>", $total_due);
                        $this->table->add_row("<strong>Total Tax (\$)</strong>", $total_tax);
                        $this->table->add_row("<strong>Total Shipping (\$)</strong>", $shipping_amount);
                        //
                        echo $this->table->generate();
                    ?>
                </div>
                <div id="chart_payment" class="span4">
                </div>                
                <div id="div_stats_breakdown">
                    <div class="span4">
                        <h4>Orders by client:</h4>
                        <?php
                            $this->table->clear();
                            foreach( $orders_clients as $client=>$orders ){
                                $this->table->add_row("<strong>$client</strong>", $orders);                                
                            }
                            echo $this->table->generate();
                        ?>
                    </div>
                    <div id="chart_clients" class="span4">
                    </div>
                    
                    <div class="span4">                    
                        <h4>Orders by Sales Rep:</h4>
                        <?php
                            $this->table->clear();
                            foreach( $orders_inspector as $client=>$orders ){                            
                                $this->table->add_row("<strong>$client</strong>", $orders);                                                        
                            }
                            echo $this->table->generate();
                        ?>
                    </div>
                    <div id="chart_inspector" class="span4">
                    </div>  
                    
                     <div class="span4">                    
                        <h4>Orders by item:</h4>
                        <?php
                            $this->table->clear();
                            foreach( $orders_items as $client=>$orders ){                           
                                $this->table->add_row("<strong>$client</strong>", $orders);                                                              
                            }
                            echo $this->table->generate();
                        ?>
                    </div>
                    <div id="chart_items" class="span4">
                    </div>  
                    
                </div>
                <?php
            } else {
                echo 'There are no orders matching your criteria';
            }
        ?>
    </div>
</div>